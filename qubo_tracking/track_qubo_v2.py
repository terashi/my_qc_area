#!/usr/bin/env python

import numpy as np
import networkx as nx
#import matplotlib.pyplot as plt

from array import array
import plot_root

# -------------------------------------
# Input file 
#input_file = 'output_tplet_menger-xy_nMu5.txt'
input_file = 'output_tplet_menger-xy_nMu50.txt'

# Event number for calculation 
event_num = 8   # = 0,1,2, ...

# Triplet selection 
#   - dtheta_diff < 0.05
dtheta_diff_cut = 0.05

Ntplet = 500

# Interaction strength
bias_weight = 1
conflict_weight = 1000  # for now...
# -------------------------------------

n_all_tplet = 0
b_ii = [] # Bias weight
b_ij = [] # Interaction strength
Q = {}    # QUBO input


genMu_R0, genMu_x0, genMu_y0 = array('d'), array('d'), array('d')

x_all, y_all, r_all, z_all = array('d'), array('d'), array('d'), array('d')
x_selected, y_selected, r_selected, z_selected = array('d'), array('d'), array('d'), array('d')
x_found, y_found, r_found, z_found = array('d'), array('d'), array('d'), array('d')


# Find line number for each event
l_Event = []
with open(input_file) as f:
    lines = f.readlines()
    lines_strip = [line.strip() for line in lines]
    l_Event = [i for i, line in enumerate(lines_strip) if 'Event' in line]
#print l_Event


with open(input_file) as f:

    print
    # Skip events to get the target event
    for l in range(l_Event[event_num]):
        skip_line = f.readline()

    # Skip Event number and generated muon information for now    
    skip_line = f.readline()  # Event
    print skip_line

    skip_line = f.readline()  # Number of GenMuons
    nmuon = int(skip_line)
    for l in range(nmuon):
        genMu_line = f.readline()
        mu_kin = genMu_line.split('\t')
        genMu_R0.append(float(mu_kin[3]))
        genMu_x0.append(float(mu_kin[4]))
        genMu_y0.append(float(mu_kin[5]))
    empty_line = f.readline()

    print genMu_R0

    seed_menger_curve = []
    seed_curve_sign = []
    seed_dtheta_avg = []
    seed_dtheta_diff = []
    seed_sl = []
    seed_l_xyz = []


    ngroup_line = f.readline()  # m_nSeedLayers
    for igroup in range(int(ngroup_line)):

        seed_line = f.readline()  # m_fitNLayer, start_layer, nSeed_LBL 
        seed = seed_line.split('\t')
        n_all_tplet += int(seed[2])

        if int(seed[0]) == 3:
            for iseed in range(int(seed[2])):
                req_line = f.readline()  # menger_curve, curve_sign, dtheta_avg, dtheta_diff
                req = req_line.split('\t')

                # dtheta_diff selection : if cut fails, skip the next 3 lines and continue
                if float(req[3]) > dtheta_diff_cut:
                    for l in range(int(seed[0])):
                        skip_line = f.readline()
                        # check all input triplets just to make plot 
                        hit = skip_line.split('\t')
                        x_all.append(float(hit[1]))
                        y_all.append(float(hit[2]))
                        r_all.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                        z_all.append(float(hit[3]))
                    continue


                seed_menger_curve.append(float(req[0]))
                seed_curve_sign.append(int(req[1]))
                seed_dtheta_avg.append(float(req[2]))
                seed_dtheta_diff.append(float(req[3]))
                seed_sl.append(int(seed[1]))

                seed_hit = []
                for il in range(int(seed[0])):
                    hit_line = f.readline()  # il, seed_x, seed_y, seed_z
                    hit = hit_line.split('\t')
                    seed_hit.append([float(hit[1]),float(hit[2]),float(hit[3])])
                    #print iseed, req[0], hit[1]
                    x_all.append(float(hit[1]))
                    y_all.append(float(hit[2]))
                    r_all.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                    z_all.append(float(hit[3]))
                    x_selected.append(float(hit[1]))
                    y_selected.append(float(hit[2]))
                    r_selected.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                    z_selected.append(float(hit[3]))

                seed_l_xyz.append(seed_hit)

        empty_line = f.readline()
            

    #print len(seed_menger_curve)
    #print seed_menger_curve[0], seed_curve_sign[0], seed_dtheta_avg[0], seed_dtheta_diff[0], seed_sl[0], seed_l_xyz[0]


    # Qubits = Triplet candidates
    num_qubits = len(seed_menger_curve)
    print "# of all triplets =", n_all_tplet, " (", len(x_all), "hits), # of qubits =", num_qubits


    #  Currently set to small value (smaller than connection or conflict weights) not to impact the energy
    for iq in range(num_qubits):
        #b_ii.append(bias_weight)
        #Q[(str(iq),str(iq))] = bias_weight
        b_ii.append(-1*(Ntplet-1)*bias_weight)
        Q[(str(iq),str(iq))] = -1*(Ntplet-1)*bias_weight

    # Find interacton strength ---
    for i in range(num_qubits):
        for j in range(num_qubits):
            if i >= j:
                continue

            #... if triplets have the same start layers ...
            if seed_sl[i] == seed_sl[j]:

                # check whether there are any shared hits 
                shared_hits = False
                for il in range(3):
                    shared = True
                    for ixyz in range(3):
                        if seed_l_xyz[i][il][ixyz] != seed_l_xyz[j][il][ixyz]:
                            shared = False
                    if shared == True:
                        shared_hits = True

                # if no shared hits --> unrelated triplets
                if shared_hits == False:
                    b_ij.append([i,j,bias_weight])
                    Q[(str(i),str(j))] = bias_weight
                # if shared hits found --> conflicting triplets
                else:
                    b_ij.append([i,j,conflict_weight+bias_weight])
                    Q[(str(i),str(j))] = conflict_weight+bias_weight
 
            #... if triplets have start layers shifted by 1 ...
            elif seed_sl[i] == seed_sl[j] - 1:
                
                # check whether there are any shared hits 
                shared_hits = False
                shared_layers = []
                for il in range(2):
                    shared = True
                    for ixyz in range(3):
                        if seed_l_xyz[i][il+1][ixyz] != seed_l_xyz[j][il][ixyz]:
                            shared = False
                    if shared == True:
                        shared_hits = True
                        shared_layers.append(il)
                        
                # if no shared hits --> unrelated triplets
                if shared_hits == False:
                    #print "unrelated : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                    b_ij.append([i,j,bias_weight])
                    Q[(str(i),str(j))] = bias_weight
                # if shared hits found 
                else:
                    #print "shared    : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                    # if shared at only one layer --> conflicting triplets
                    if len(shared_layers) == 1:
                        #print "shared(1)" #         : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                        b_ij.append([i,j,conflict_weight+bias_weight])
                        Q[(str(i),str(j))] = conflict_weight+bias_weight

                    # if shared at two layers --> possible quadruplet candidates
                    elif len(shared_layers) == 2:                
                        #print "shared(2)     : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                        # check menger curvatures and signs
                        dcurve = abs(seed_menger_curve[i] - seed_menger_curve[j])
                        dcurve_sign = seed_curve_sign[i] * seed_curve_sign[j]
                        drz = abs(seed_dtheta_avg[i] - seed_dtheta_avg[j])

                        # if menger curvatures have opposite sign --> conflicting triplets (OK?)
                        if dcurve_sign == -1:
                            #print "shared(2,invalid)" # : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                            b_ij.append([i,j,conflict_weight+bias_weight])
                            Q[(str(i),str(j))] = conflict_weight+bias_weight
                        # if curvatures are consistent --> good quadruplet candidates
                        else:
                            s_ij = -1 * (1 - 0.5 * (dcurve + drz))
                            b_ij.append([i,j,s_ij+bias_weight])
                            Q[(str(i),str(j))] = s_ij+bias_weight
                            #print "shared(2,valid)    --> s_ij = ", s_ij #   : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                            
                    else:
                        print "shared(other)    : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                                                    

            #... if triplets have start layers shifted by 2 or more ...
            elif seed_sl[i] <= seed_sl[j] - 2:
                b_ij.append([i,j,bias_weight])  # --> unrelated triplets
                Q[(str(i),str(j))] = bias_weight


print "# of interaction weights = ", len(b_ij), ", bias weights = ", len(b_ii), ", Q size = ", len(Q)
print 

    
# === Solving QUBO problem =====================
from dwave_qbsolv import QBSolv
import neal

# Setup simulated anealing 
sampler = neal.SimulatedAnnealingSampler()

# Setup qbsolv
subqubo_size = 30
response = QBSolv().sample_qubo(Q, solver=sampler, num_repeats=50, solver_limit=subqubo_size)
#print("energies=" + str(list(response.data_vectors['energy'])))   

i = 0
for datum in response.data(['sample', 'energy', 'num_occurrences']):   
    # the lowest energu state
    if i == 0:
        ntplet_found = 0
        print "Lowest energy state:"
        print(datum.sample)
        for iq in range(num_qubits):
            if datum.sample[str(iq)] == 1:
                #print seed_l_xyz[iq]
                for il in range(3):
                    x_found.append(seed_l_xyz[iq][il][0])
                    y_found.append(seed_l_xyz[iq][il][1])
                    r_found.append((seed_l_xyz[iq][il][0]*seed_l_xyz[iq][il][0]+seed_l_xyz[iq][il][1]*seed_l_xyz[iq][il][1])**0.5)
                    z_found.append(seed_l_xyz[iq][il][2])
                ntplet_found += 1
        print "# of triplets found after QUBO at lowest energy =", ntplet_found
        print 
        print "First 20 low energy states:"

    # 20 lowest energy states
    if i < 20:
        ntplet_found = 0
        for iq in range(num_qubits):
            if datum.sample[str(iq)] == 1:
                ntplet_found += 1
        #print(datum.sample, "Energy: ", datum.energy, "Occurrences: ", datum.num_occurrences)
        print("Energy: ", datum.energy, "Occurrences: ", datum.num_occurrences, "#Triplets: ", ntplet_found)
        
    i += 1


print
#p4,p5,p6,p7,p8,p9,p10,p11 = plot_root.checkEff(x_found, y_found, z_found)


plot_root.plotGraph('track', Ntplet, genMu_R0, genMu_x0, genMu_y0, 
                    x_all, y_all, r_all, z_all, 
                    x_selected, y_selected, r_selected, z_selected, 
                    x_found, y_found, r_found, z_found,  
                    1300, 1300, 1300, 1000)
plot_root.plotGraph('inner_track', Ntplet, genMu_R0, genMu_x0, genMu_y0, 
                    x_all, y_all, r_all, z_all, 
                    x_selected, y_selected, r_selected, z_selected, 
                    x_found, y_found, r_found, z_found, 
                    200, 200, 200, 200)


'''
from dimod.reference.samplers import ExactSolver
sampler = ExactSolver()
response = sampler.sample_qubo(Q)
i=0
for datum in response.data(['sample', 'energy', 'num_occurrences']):   
    if i < 20:
        print(datum.sample, "Energy: ", datum.energy, "Occurrences: ", datum.num_occurrences)
    i += 1
'''
    
    
