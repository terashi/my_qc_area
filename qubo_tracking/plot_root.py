from ROOT import TCanvas, TColor, TGaxis, TH1F, TGraph, gPad, TLine
from ROOT import kBlack, kBlue, kRed, kGray
from array import array

zero = array('d')

def plotGraph(name,ntplet,R0,x0,y0,x,y,r,z,
              x1=zero,y1=zero,r1=zero,z1=zero,
              x2=zero,y2=zero,r2=zero,z2=zero,
              xrange=1500,yrange=1500,rrange=1500,zrange=1000):

    l1 = TLine()
    l2 = TLine()
    l1.SetLineWidth(1)
    l2.SetLineWidth(1)

    n = len(x)
    gr_xy = TGraph(n,x,y)
    gr_xy.SetTitle("")
    gr_rz = TGraph(n,z,r)
    gr_rz.SetTitle("")

    n1 = len(x1)
    n2 = len(x2)
    if n1 > 0:
        gr1_xy = TGraph(n1,x1,y1)
        gr1_xy.SetTitle("")
        gr1_rz = TGraph(n1,z1,r1)
        gr1_rz.SetTitle("")
        if n2 > 0:
            gr2_xy = TGraph(n2,x2,y2)
            gr2_xy.SetTitle("")
            gr2_rz = TGraph(n2,z2,r2)
            gr2_rz.SetTitle("")
            

    c1 = TCanvas("c1", "canvas", 800, 770)
    c1.cd()
    gPad.SetTopMargin(0.06);
    gPad.SetBottomMargin(0.14);
    gPad.SetLeftMargin(0.15);
    gPad.SetRightMargin(0.04);

    gr_xy.GetHistogram().GetXaxis().SetLimits(-1*xrange,xrange)
    gr_xy.GetHistogram().GetYaxis().SetRangeUser(-1*yrange,yrange)
    gr_xy.GetHistogram().SetXTitle("x [mm]")
    gr_xy.GetHistogram().SetYTitle("y [mm]")
    gr_xy.GetHistogram().SetTitleOffset(1.0,"X")
    gr_xy.GetHistogram().SetTitleOffset(1.4,"Y")
    gr_xy.GetHistogram().SetTitleSize(0.05,"X")
    gr_xy.GetHistogram().SetTitleSize(0.05,"Y")
    gr_xy.SetMarkerColor(kGray+1)
    gr_xy.SetMarkerStyle(24)
    gr_xy.SetMarkerSize(1.6)
    gr_xy.Draw("AP");
    if n1 > 0:
        gr1_xy.SetMarkerStyle(25)
        gr1_xy.SetMarkerColor(kBlue)
        gr1_xy.SetMarkerSize(1.0)
        gr1_xy.Draw("P");
        if n2 > 0:
            gr2_xy.SetMarkerStyle(20)
            gr2_xy.SetMarkerColor(kRed)
            gr2_xy.SetMarkerSize(0.7)
            for iq in range(len(x2)/3):
                l1.DrawLine(x2[iq*3],y2[iq*3],x2[iq*3+1],y2[iq*3+1])
                l2.DrawLine(x2[iq*3+1],y2[iq*3+1],x2[iq*3+2],y2[iq*3+2])
            gr2_xy.Draw("P");

    #gr_xy.Draw("P");        
    c1.Update()
    if ntplet > 0:
        c1.SaveAs(name+"_xy_"+str(ntplet)+"tplet.pdf")
        c1.SaveAs(name+"_xy_"+str(ntplet)+"tplet.png")
    else:
        c1.SaveAs(name+"_xy.pdf")
        c1.SaveAs(name+"_xy.png")


    c2 = TCanvas("c2", "canvas", 800, 600)
    c2.cd()
    gPad.SetTopMargin(0.06);
    gPad.SetBottomMargin(0.14);
    gPad.SetLeftMargin(0.15);
    gPad.SetRightMargin(0.04);

    gr_rz.GetHistogram().GetXaxis().SetLimits(-1*zrange,zrange)
    gr_rz.GetHistogram().GetYaxis().SetRangeUser(0,rrange)
    gr_rz.GetHistogram().SetXTitle("z [mm]")
    gr_rz.GetHistogram().SetYTitle("r [mm]")
    gr_rz.GetHistogram().SetTitleOffset(1.0,"X")
    gr_rz.GetHistogram().SetTitleOffset(1.4,"Y")
    gr_rz.GetHistogram().SetTitleSize(0.05,"X")
    gr_rz.GetHistogram().SetTitleSize(0.05,"Y")
    gr_rz.SetMarkerColor(kGray+1)
    gr_rz.SetMarkerStyle(24)
    gr_rz.SetMarkerSize(1.6)
    gr_rz.Draw("AP");
    if n1 > 0:
        gr1_rz.SetMarkerStyle(25)
        gr1_rz.SetMarkerColor(kBlue)
        gr1_rz.SetMarkerSize(1.0)
        gr1_rz.Draw("P");
        if n2 > 0:
            gr2_rz.SetMarkerStyle(20)
            gr2_rz.SetMarkerColor(kRed)
            gr2_rz.SetMarkerSize(0.7)
            for iq in range(len(x2)/3):
                l1.DrawLine(z2[iq*3],r2[iq*3],z2[iq*3+1],r2[iq*3+1])
                l2.DrawLine(z2[iq*3+1],r2[iq*3+1],z2[iq*3+2],r2[iq*3+2])
            gr2_rz.Draw("P");
    #gr_rz.Draw("P");        
    c2.Update()
    if ntplet > 0:
        c2.SaveAs(name+"_rz_"+str(ntplet)+"tplet.pdf")
        c2.SaveAs(name+"_rz_"+str(ntplet)+"tplet.png")
    else:
        c2.SaveAs(name+"_rz.pdf")
        c2.SaveAs(name+"_rz.png")



def checkEff(x2,y2,z2):

    # Forming "4-ples"
    p4 = []
    for iq1 in range(len(x2)/3):
        p4_hit = []
        for iq2 in range(len(x2)/3):
            if iq1 >= iq2:
                continue
            # If 4-hits found
            if x2[iq1*3+1] == x2[iq2*3] and y2[iq1*3+1] == y2[iq2*3] and z2[iq1*3+1] == z2[iq2*3] and x2[iq1*3+2] == x2[iq2*3+1] and y2[iq1*3+2] == y2[iq2*3+1] and z2[iq1*3+2] == z2[iq2*3+1]:
                p4_hit.append([x2[iq1*3],y2[iq1*3],z2[iq1*3]])
                p4_hit.append([x2[iq1*3+1],y2[iq1*3+1],z2[iq1*3+1]])
                p4_hit.append([x2[iq1*3+2],y2[iq1*3+2],z2[iq1*3+2]])
                p4_hit.append([x2[iq2*3+2],y2[iq2*3+2],z2[iq2*3+2]])
                p4_hit.append(iq1)
                p4_hit.append(iq2)
        if len(p4_hit) == 6:
            p4.append(p4_hit)

    print " 4-hits = ",len(p4)
    #for iqp in range(len(p4)):
    #    print p4[iqp]


    # Forming "5-ples"
    p5 = []
    for iqp in range(len(p4)):
        p5_hit = []
        for iq in range(len(x2)/3):
            if p4[iqp][4] >= iq or p4[iqp][5] >= iq:
                continue
            # If contiguous 5-hits found
            if p4[iqp][2][0] == x2[iq*3] and p4[iqp][2][1] == y2[iq*3] and p4[iqp][2][2] == z2[iq*3] and p4[iqp][3][0] == x2[iq*3+1] and p4[iqp][3][1] == y2[iq*3+1] and p4[iqp][3][2] == z2[iq*3+1]:
                for il in range(4):
                    p5_hit.append([p4[iqp][il][0],p4[iqp][il][1],p4[iqp][il][2]])
                p5_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p5_hit.append(p4[iqp][4])
                p5_hit.append(p4[iqp][5])
                p5_hit.append(iq)
        if len(p5_hit) == 8:
            p5.append(p5_hit)
            
    print " 5-hits = ",len(p5)
    #for iqp in range(len(p5)):
    #    print p5[iqp]


    # Forming "6-ples"
    p6 = []
    for iqp in range(len(p5)):
        p6_hit = []
        for iq in range(len(x2)/3):
            if p5[iqp][5] >= iq or p5[iqp][6] >= iq or p5[iqp][7] >= iq:
                continue
            # If contiguous 6-hits found
            if p5[iqp][3][0] == x2[iq*3] and p5[iqp][3][1] == y2[iq*3] and p5[iqp][3][2] == z2[iq*3] and p5[iqp][4][0] == x2[iq*3+1] and p5[iqp][4][1] == y2[iq*3+1] and p5[iqp][4][2] == z2[iq*3+1]:
                for il in range(5):
                    p6_hit.append([p5[iqp][il][0],p5[iqp][il][1],p5[iqp][il][2]])
                p6_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p6_hit.append(p5[iqp][5])
                p6_hit.append(p5[iqp][6])
                p6_hit.append(p5[iqp][7])
                p6_hit.append(iq)
        if len(p6_hit) == 10:
            p6.append(p6_hit)
            
    print " 6-hits = ",len(p6)
    #for iqp in range(len(p6)):
    #    print p6[iqp]


    # Forming "7-ples"
    p7 = []
    for iqp in range(len(p6)):
        p7_hit = []
        for iq in range(len(x2)/3):
            if p6[iqp][6] >= iq or p6[iqp][7] >= iq or p6[iqp][8] >= iq or p6[iqp][9] >= iq:
                continue
            # If contiguous 7-hits found
            if p6[iqp][4][0] == x2[iq*3] and p6[iqp][4][1] == y2[iq*3] and p6[iqp][4][2] == z2[iq*3] and p6[iqp][5][0] == x2[iq*3+1] and p6[iqp][5][1] == y2[iq*3+1] and p6[iqp][5][2] == z2[iq*3+1]:
                for il in range(6):
                    p7_hit.append([p6[iqp][il][0],p6[iqp][il][1],p6[iqp][il][2]])
                p7_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p7_hit.append(p6[iqp][6])
                p7_hit.append(p6[iqp][7])
                p7_hit.append(p6[iqp][8])
                p7_hit.append(p6[iqp][9])
                p7_hit.append(iq)
        if len(p7_hit) == 12:
            p7.append(p7_hit)
            
    print " 7-hits = ",len(p7)
    #for iqp in range(len(p7)):
    #    print p7[iqp]


    # Forming "8-ples"
    p8 = []
    for iqp in range(len(p7)):
        p8_hit = []
        for iq in range(len(x2)/3):
            if p7[iqp][7] >= iq or p7[iqp][8] >= iq or p7[iqp][9] >= iq or p7[iqp][10] >= iq or p7[iqp][11] >= iq:
                continue
            # If contiguous 8-hits found
            if p7[iqp][5][0] == x2[iq*3] and p7[iqp][5][1] == y2[iq*3] and p7[iqp][5][2] == z2[iq*3] and p7[iqp][6][0] == x2[iq*3+1] and p7[iqp][6][1] == y2[iq*3+1] and p7[iqp][6][2] == z2[iq*3+1]:
                for il in range(7):
                    p8_hit.append([p7[iqp][il][0],p7[iqp][il][1],p7[iqp][il][2]])
                p8_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p8_hit.append(p7[iqp][7])
                p8_hit.append(p7[iqp][8])
                p8_hit.append(p7[iqp][9])
                p8_hit.append(p7[iqp][10])
                p8_hit.append(p7[iqp][11])
                p8_hit.append(iq)
        if len(p8_hit) == 14:
            p8.append(p8_hit)
            
    print " 8-hits = ",len(p8)
    #for iqp in range(len(p8)):
    #    print p8[iqp]


    # Forming "9-ples"
    p9 = []
    for iqp in range(len(p8)):
        p9_hit = []
        for iq in range(len(x2)/3):
            if p8[iqp][8] >= iq or p8[iqp][9] >= iq or p8[iqp][10] >= iq or p8[iqp][11] >= iq or p8[iqp][12] >= iq or p8[iqp][13] >= iq:
                continue
            # If contiguous 9-hits found
            if p8[iqp][6][0] == x2[iq*3] and p8[iqp][6][1] == y2[iq*3] and p8[iqp][6][2] == z2[iq*3] and p8[iqp][7][0] == x2[iq*3+1] and p8[iqp][7][1] == y2[iq*3+1] and p8[iqp][7][2] == z2[iq*3+1]:
                for il in range(8):
                    p9_hit.append([p8[iqp][il][0],p8[iqp][il][1],p8[iqp][il][2]])
                p9_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p9_hit.append(p8[iqp][8])
                p9_hit.append(p8[iqp][9])
                p9_hit.append(p8[iqp][10])
                p9_hit.append(p8[iqp][11])
                p9_hit.append(p8[iqp][12])
                p9_hit.append(p8[iqp][13])
                p9_hit.append(iq)
        if len(p9_hit) == 16:
            p9.append(p9_hit)
            
    print " 9-hits = ",len(p9)
    #for iqp in range(len(p9)):
    #    print p9[iqp]


    # Forming "10-ples"
    p10 = []
    for iqp in range(len(p9)):
        p10_hit = []
        for iq in range(len(x2)/3):
            if p9[iqp][9] >= iq or p9[iqp][10] >= iq or p9[iqp][11] >= iq or p9[iqp][12] >= iq or p9[iqp][13] >= iq or p9[iqp][14] >= iq or p9[iqp][15] >= iq:
                continue
            # If contiguous 10-hits found
            if p9[iqp][7][0] == x2[iq*3] and p9[iqp][7][1] == y2[iq*3] and p9[iqp][7][2] == z2[iq*3] and p9[iqp][8][0] == x2[iq*3+1] and p9[iqp][8][1] == y2[iq*3+1] and p9[iqp][8][2] == z2[iq*3+1]:
                for il in range(9):
                    p10_hit.append([p9[iqp][il][0],p9[iqp][il][1],p9[iqp][il][2]])
                p10_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p10_hit.append(p9[iqp][9])
                p10_hit.append(p9[iqp][10])
                p10_hit.append(p9[iqp][11])
                p10_hit.append(p9[iqp][12])
                p10_hit.append(p9[iqp][13])
                p10_hit.append(p9[iqp][14])
                p10_hit.append(p9[iqp][15])
                p10_hit.append(iq)
        if len(p10_hit) == 18:
            p10.append(p10_hit)
            
    print "10-hits = ",len(p10)
#    for iqp in range(len(p10)):
#        print "   ",p10[iqp][10:]


    # Forming "11-ples"
    p11 = []
    for iqp in range(len(p10)):
        p11_hit = []
        for iq in range(len(x2)/3):
            if p10[iqp][10] >= iq or p10[iqp][11] >= iq or p10[iqp][12] >= iq or p10[iqp][13] >= iq or p10[iqp][14] >= iq or p10[iqp][15] >= iq or p10[iqp][16] >= iq or p10[iqp][17] >= iq:
                continue
            # If contiguous 11-hits found
            if p10[iqp][8][0] == x2[iq*3] and p10[iqp][8][1] == y2[iq*3] and p10[iqp][8][2] == z2[iq*3] and p10[iqp][9][0] == x2[iq*3+1] and p10[iqp][9][1] == y2[iq*3+1] and p10[iqp][9][2] == z2[iq*3+1]:
                for il in range(10):
                    p11_hit.append([p10[iqp][il][0],p10[iqp][il][1],p10[iqp][il][2]])
                p11_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p11_hit.append(p10[iqp][10])
                p11_hit.append(p10[iqp][11])
                p11_hit.append(p10[iqp][12])
                p11_hit.append(p10[iqp][13])
                p11_hit.append(p10[iqp][14])
                p11_hit.append(p10[iqp][15])
                p11_hit.append(p10[iqp][16])
                p11_hit.append(p10[iqp][17])
                p11_hit.append(iq)
        if len(p11_hit) == 20:
            p11.append(p11_hit)
            
    print "11-hits = ",len(p11)
#    for iqp in range(len(p11)):
#        print "   ",p11[iqp][11:]


    # Forming "12-ples"
    p12 = []
    for iqp in range(len(p11)):
        p12_hit = []
        for iq in range(len(x2)/3):
            if p11[iqp][11] >= iq or p11[iqp][12] >= iq or p11[iqp][13] >= iq or p11[iqp][14] >= iq or p11[iqp][15] >= iq or p11[iqp][16] >= iq or p11[iqp][17] >= iq or p11[iqp][18] >= iq or p11[iqp][19] >= iq:
                continue
            # If contiguous 12-hits found
            if p11[iqp][9][0] == x2[iq*3] and p11[iqp][9][1] == y2[iq*3] and p11[iqp][9][2] == z2[iq*3] and p11[iqp][10][0] == x2[iq*3+1] and p11[iqp][10][1] == y2[iq*3+1] and p11[iqp][10][2] == z2[iq*3+1]:
                for il in range(11):
                    p12_hit.append([p11[iqp][il][0],p11[iqp][il][1],p11[iqp][il][2]])
                p12_hit.append([x2[iq*3+2],y2[iq*3+2],z2[iq*3+2]])
                p12_hit.append(p11[iqp][11])
                p12_hit.append(p11[iqp][12])
                p12_hit.append(p11[iqp][13])
                p12_hit.append(p11[iqp][14])
                p12_hit.append(p11[iqp][15])
                p12_hit.append(p11[iqp][16])
                p12_hit.append(p11[iqp][17])
                p12_hit.append(p11[iqp][18])
                p12_hit.append(p11[iqp][19])
                p12_hit.append(iq)
        if len(p12_hit) == 22:
            p12.append(p12_hit)
            
    print "12-hits = ",len(p12)
    #for iqp in range(len(p12)):
    #    print p12[iqp]


    # "10-ps" only
    p10_only = []
    for iqp1 in range(len(p10)):
#        print "  ",p10[iqp1][10:]
        match = False
        for iqp2 in range(len(p11)):            
#            print "    ",p11[iqp2][11:]
            for k in range(2):
                match_all = False
                for i in range(8):
                    if i == 0 and p10[iqp1][10] == p11[iqp2][11+k]:
                        match_all = True
                    else:
                        if p10[iqp1][10+i] != p11[iqp2][11+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        if match == False:
            p10_only.append(p10[iqp1])
#        print iqp1, match        
    nevt = len(p11)+len(p10_only)
    print "10-hits only = ", len(p10_only), " (",nevt,")"

    # "9-ps" only
    p9_only = []
    for iqp1 in range(len(p9)):
#        print "  ",p9[iqp1][9:]
        match = False
        for iqp2 in range(len(p10_only)):            
#            print "    ",p10_only[iqp2][10:]
            for k in range(2):
                match_all = False
                for i in range(7):
                    if i == 0 and p9[iqp1][9] == p10_only[iqp2][10+k]:
                        match_all = True
                    else:
                        if p9[iqp1][9+i] != p10_only[iqp2][10+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p11)):            
#            print "      ",p11[iqp2][11:]
            for k in range(3):
                match_all = False
                for i in range(7):
                    if i == 0 and p9[iqp1][9] == p11[iqp2][11+k]:
                        match_all = True
                    else:
                        if p9[iqp1][9+i] != p11[iqp2][11+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        if match == False:
            p9_only.append(p9[iqp1])
        #print iqp1, match        
    nevt += len(p9_only)
    print " 9-hits only = ", len(p9_only), " (",nevt,")"

    # "8-ps" only
    p8_only = []
    for iqp1 in range(len(p8)):
        match = False
        for iqp2 in range(len(p9_only)):            
            for k in range(2):
                match_all = False
                for i in range(6):
                    if i == 0 and p8[iqp1][8] == p9_only[iqp2][9+k]:
                        match_all = True
                    else:
                        if p8[iqp1][8+i] != p9_only[iqp2][9+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p10_only)):            
            for k in range(3):
                match_all = False
                for i in range(6):
                    if i == 0 and p8[iqp1][8] == p10_only[iqp2][10+k]:
                        match_all = True
                    else:
                        if p8[iqp1][8+i] != p10_only[iqp2][10+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p11)):            
            for k in range(4):
                match_all = False
                for i in range(6):
                    if i == 0 and p8[iqp1][8] == p11[iqp2][11+k]:
                        match_all = True
                    else:
                        if p8[iqp1][8+i] != p11[iqp2][11+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        if match == False:
            p8_only.append(p8[iqp1])
        #print iqp1, match        
    nevt += len(p8_only)
    print " 8-hits only = ", len(p8_only), " (",nevt,")"

    # "7-ps" only
    p7_only = []
    for iqp1 in range(len(p7)):
        match = False
        for iqp2 in range(len(p8_only)):            
            for k in range(2):
                match_all = False
                for i in range(5):
                    if i == 0 and p7[iqp1][7] == p8_only[iqp2][8+k]:
                        match_all = True
                    else:
                        if p7[iqp1][7+i] != p8_only[iqp2][8+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p9_only)):            
            for k in range(3):
                match_all = False
                for i in range(5):
                    if i == 0 and p7[iqp1][7] == p9_only[iqp2][9+k]:
                        match_all = True
                    else:
                        if p7[iqp1][7+i] != p9_only[iqp2][9+k+i]:
                            match_all = False
                    if match_all == True:
                        match = True
                        continue
        for iqp2 in range(len(p10_only)):            
            for k in range(4):
                match_all = False
                for i in range(5):
                    if i == 0 and p7[iqp1][7] == p10_only[iqp2][10+k]:
                        match_all = True
                    else:
                        if p7[iqp1][7+i] != p10_only[iqp2][10+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p11)):            
            for k in range(5):
                match_all = False
                for i in range(5):
                    if i == 0 and p7[iqp1][7] == p11[iqp2][11+k]:
                        match_all = True
                    else:
                        if p7[iqp1][7+i] != p11[iqp2][11+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        if match == False:
            p7_only.append(p7[iqp1])
        #print iqp1, match        
    nevt += len(p7_only)
    print " 7-hits only = ", len(p7_only), " (",nevt,")"

    # "6-ps" only
    p6_only = []
    for iqp1 in range(len(p6)):
        match = False
        for iqp2 in range(len(p7_only)):            
            for k in range(2):
                match_all = False
                for i in range(4):
                    if i == 0 and p6[iqp1][6] == p7_only[iqp2][7+k]:
                        match_all = True
                    else:
                        if p6[iqp1][6+i] != p7_only[iqp2][7+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p8_only)):            
            for k in range(3):
                match_all = False
                for i in range(4):
                    if i == 0 and p6[iqp1][6] == p8_only[iqp2][8+k]:
                        match_all = True
                    else:
                        if p6[iqp1][6+i] != p8_only[iqp2][8+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p9_only)):            
            for k in range(4):
                match_all = False
                for i in range(4):
                    if i == 0 and p6[iqp1][6] == p9_only[iqp2][9+k]:
                        match_all = True
                    else:
                        if p6[iqp1][6+i] != p9_only[iqp2][9+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                continue
        for iqp2 in range(len(p10_only)):            
            for k in range(5):
                match_all = False
                for i in range(4):
                    if i == 0 and p6[iqp1][6] == p10_only[iqp2][10+k]:
                        match_all = True
                    else:
                        if p6[iqp1][6+i] != p10_only[iqp2][10+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p11)):            
            for k in range(6):
                match_all = False
                for i in range(4):
                    if i == 0 and p6[iqp1][6] == p11[iqp2][11+k]:
                        match_all = True
                    else:
                        if p6[iqp1][6+i] != p11[iqp2][11+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        if match == False:
            p6_only.append(p6[iqp1])
        #print iqp1, match        
    nevt += len(p6_only)
    print " 6-hits only = ", len(p6_only), " (",nevt,")"

    # "5-ps" only
    p5_only = []
    for iqp1 in range(len(p5)):
        match = False
        for iqp2 in range(len(p6_only)):            
            for k in range(2):
                match_all = False
                for i in range(3):
                    if i == 0 and p5[iqp1][5] == p6_only[iqp2][6+k]:
                        match_all = True
                    else:
                        if p5[iqp1][5+i] != p6_only[iqp2][6+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p7_only)):            
            for k in range(3):
                match_all = False
                for i in range(3):
                    if i == 0 and p5[iqp1][5] == p7_only[iqp2][7+k]:
                        match_all = True
                    else:
                        if p5[iqp1][5+i] != p7_only[iqp2][7+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p8_only)):            
            for k in range(4):
                match_all = False
                for i in range(3):
                    if i == 0 and p5[iqp1][5] == p8_only[iqp2][8+k]:
                        match_all = True
                    else:
                        if p5[iqp1][5+i] != p8_only[iqp2][8+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p9_only)):            
            for k in range(5):
                match_all = False
                for i in range(3):
                    if i == 0 and p5[iqp1][5] == p9_only[iqp2][9+k]:
                        match_all = True
                    else:
                        if p5[iqp1][5+i] != p9_only[iqp2][9+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p10_only)):            
            for k in range(6):
                match_all = False
                for i in range(3):
                    if i == 0 and p5[iqp1][5] == p10_only[iqp2][10+k]:
                        match_all = True
                    else:
                        if p5[iqp1][5+i] != p10_only[iqp2][10+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p11)):            
            for k in range(7):
                match_all = False
                for i in range(3):
                    if i == 0 and p5[iqp1][5] == p11[iqp2][11+k]:
                        match_all = True
                    else:
                        if p5[iqp1][5+i] != p11[iqp2][11+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        if match == False:
            p5_only.append(p5[iqp1])
        #print iqp1, match        
    nevt += len(p5_only)
    print " 5-hits only = ", len(p5_only), " (",nevt,")"

    # "4-ps" only
    p4_only = []
    for iqp1 in range(len(p4)):
        match = False
        for iqp2 in range(len(p5_only)):            
            for k in range(2):
                match_all = False
                for i in range(2):
                    if i == 0 and p4[iqp1][4] == p5_only[iqp2][5+k]:
                        match_all = True
                    else:
                        if p4[iqp1][4+i] != p5_only[iqp2][5+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p6_only)):            
            for k in range(3):
                match_all = False
                for i in range(2):
                    if i == 0 and p4[iqp1][4] == p6_only[iqp2][6+k]:
                        match_all = True
                    else:
                        if p4[iqp1][4+i] != p6_only[iqp2][6+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p7_only)):            
            for k in range(4):
                match_all = False
                for i in range(2):
                    if i == 0 and p4[iqp1][4] == p7_only[iqp2][7+k]:
                        match_all = True
                    else:
                        if p4[iqp1][4+i] != p7_only[iqp2][7+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p8_only)):            
            for k in range(5):
                match_all = False
                for i in range(2):
                    if i == 0 and p4[iqp1][4] == p8_only[iqp2][8+k]:
                        match_all = True
                    else:
                        if p4[iqp1][4+i] != p8_only[iqp2][8+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p9_only)):            
            for k in range(6):
                match_all = False
                for i in range(2):
                    if i == 0 and p4[iqp1][4] == p9_only[iqp2][9+k]:
                        match_all = True
                    else:
                        if p4[iqp1][4+i] != p9_only[iqp2][9+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p10_only)):            
            for k in range(7):
                match_all = False
                for i in range(2):
                    if i == 0 and p4[iqp1][4] == p10_only[iqp2][10+k]:
                        match_all = True
                    else:
                        if p4[iqp1][4+i] != p10_only[iqp2][10+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        for iqp2 in range(len(p11)):            
            for k in range(8):
                match_all = False
                for i in range(2):
                    if i == 0 and p4[iqp1][4] == p11[iqp2][11+k]:
                        match_all = True
                    else:
                        if p4[iqp1][4+i] != p11[iqp2][11+k+i]:
                            match_all = False
                if match_all == True:
                    match = True
                    continue
        if match == False:
            p4_only.append(p4[iqp1])
        #print iqp1, match        
    nevt += len(p4_only)
    print " 4-hits only = ", len(p4_only), " (",nevt,")"


    return p4_only,p5_only,p6_only,p7_only,p8_only,p9_only,p10_only,p11
