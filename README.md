This is my personal area to study simulation for quantum computing. 


[QISKit](https://qiskit.org) by IBM ([tutorial](https://nbviewer.jupyter.org/github/QISKit/qiskit-tutorial/blob/master/index.ipynb)) and [Cirq](https://cirq.readthedocs.io/en/latest/index.html) by Google ([Cirq](https://github.com/quantumlib/Cirq) in quantumlib).

QISKit [インストールメモ](https://www.evernote.com/l/Aa2rur-JGBFGPLuTXF3cBMlaE7gGp5mjlk4)
  - grover_n3.py (Grover algorithm with 3-qubits system)
    - copied [grover.py (2-qubits)](https://github.com/quantumlib/Cirq/blob/master/examples/grover.py) from quantumlib/Cirq
    - modified according to the [3-qubit Grover algorithm example](https://nbviewer.jupyter.org/github/QISKit/qiskit-tutorial/blob/master/reference/algorithms/grover_algorithm.ipynb) in QISKit
  

Tracking with quantum computing :
  - Dataset preparation described in Instructions_track_preparation.txt

  - Example tracking with D-Wave annealing system:
      - Instructions described in Instructions_track_QUBO.txt
      - track_qubo(_v2).py : main script to run annealing+qbsolv to solve QUBO for tracking
      - plot_root.py : macro to plot hits found by QUBO
      - output_tplet_menger-xy_nMu50.txt : input file containing triplet information

  - Example tracking with IBM gate system:
      - Instructions described in Instructions_track_Gate.txt
      - track_ising(_v2).py : main script to run optimization with Ising hamiltonian for tracking
      - ising_tracking.py : Ising hamiltonian is defined here
      - plot_results(_v2).py : macro to plot hits found
      - output_tplet_menger-xy_nMu5_Eta....txt : input files containing triplet information
