import cirq
import math
import numpy as np
import matplotlib.pyplot as plt
from cirq import GridQubit, X, CNOT, TOFFOLI, ry
from cirq.contrib.svg import SVGCircuit, circuit_to_svg
import sys

early = cirq.InsertStrategy.EARLIEST
new = cirq.InsertStrategy.NEW_THEN_INLINE


# Define splitting functions and sudakov factors

def P_f(t, g):
    alpha = g**2 * Phat_f(t)/ (4 * math.pi)
    return alpha

def Phat_f(t):
    return math.log(t)

def Phat_bos(t):
    return math.log(t)

def Delta_f(t, g):
    return math.exp(P_f(t,g))

def P_bos(t, g_a, g_b):
    alpha = g_a**2 * Phat_bos(t)/ (4 * math.pi) + g_b**2 * Phat_bos(t)/ (4 * math.pi)
    return alpha

def Delta_bos(t, g_a, g_b):
    return math.exp(P_bos(t, g_a, g_b))


def populateParameterLists(N, timeStepList, P_aList, P_bList, P_phiList, Delta_aList, Delta_bList, Delta_phiList, g_a,
                           g_b, eps):
    """Populates the 6 lists with correct values for each time step theta"""
    for i in range(N):
        # Compute time steps
        t_up = eps ** ((i) / N)
        t_mid = eps ** ((i + 0.5) / N)
        t_low = eps ** ((i + 1) / N)
        timeStepList.append(t_mid)
        # Compute values for emission matrices   
        Delta_a = Delta_f(t_low, g_a)/ Delta_f(t_up, g_a)
        Delta_b = Delta_f(t_low, g_b) / Delta_f(t_up, g_b)
        Delta_phi = Delta_bos(t_low, g_a, g_b) / Delta_bos(t_up, g_a, g_b)
        P_a, P_b, P_phi = P_f(t_mid, g_a), P_f(t_mid, g_b), P_bos(t_mid, g_a, g_b)

        # Add them to the list
        P_aList.append(P_a)
        P_bList.append(P_b)
        P_phiList.append(P_phi)
        Delta_aList.append(Delta_a)
        Delta_bList.append(Delta_b)
        Delta_phiList.append(Delta_phi)

def allocateQubs(N, n_i, L, pReg, hReg, w_hReg, eReg, wReg, n_aReg, w_aReg, n_bReg, w_bReg, n_phiReg, w_phiReg):
    """method 1: Pro: keeps qubits geometrically close (in rectangle), Con: Ordering is weird so hard to debug"""
    pReg.extend([[GridQubit(i,j) for j in range(3)] for i in range(N+n_i)])
    hReg.extend([[GridQubit(i,j) for j in range(4, 4+L)] for i in range(N)])
    w_hReg.extend([GridQubit(N+n_i,j) for j in range(L-1)])
    eReg.extend([GridQubit(N+n_i,L-1)])
    wReg.extend([GridQubit(N+n_i,j) for j in range(L,L+5)])
    n_phiReg.extend([GridQubit(N+n_i+1,j) for j in range(L)])
    w_phiReg.extend([GridQubit(N+n_i+1,j) for j in range(L,2*L-1)])
    n_aReg.extend([GridQubit(N+n_i+2,j) for j in range(L)])
    w_aReg.extend([GridQubit(N+n_i+2,j) for j in range(L,2*L-1)])
    n_bReg.extend([GridQubit(N+n_i+3,j) for j in range(L)])
    w_bReg.extend([GridQubit(N+n_i+3,j) for j in range(L,2*L-1)])

def intializeParticles(circuit, pReg, initialParticles):
    """ Apply appropriate X gates to ensure that the p register contains all of the initial particles.
        The p registers contains particles in the form of a list [LSB, middle bit, MSB]"""
    for currentParticleIndex in range(len(initialParticles)):
        for particleBit in range(3):
            if initialParticles[currentParticleIndex][particleBit] == 1:
                circuit.append(X(pReg[currentParticleIndex][particleBit]), strategy=early)

def flavorControl(circuit, flavor, control, target, ancilla):
    """Controlled x onto targetQubit if "control" particle is of the correct flavor"""
    if flavor == "phi":
        circuit.append([X(control[1]), X(control[2])], strategy=new)
        circuit.append(TOFFOLI(control[0], control[1], ancilla), strategy=new)
        circuit.append(TOFFOLI(control[2], ancilla, target), strategy=new)
        # undo work
        circuit.append(TOFFOLI(control[0], control[1], ancilla), strategy=new)
        circuit.append([X(control[1]), X(control[2])], strategy=new)
    if flavor == "a":
        circuit.append(X(control[0]), strategy=new)
        circuit.append(TOFFOLI(control[0], control[2], target), strategy=new)
        # undo work
        circuit.append(X(control[0]), strategy=new)
    if flavor == "b":
        circuit.append(TOFFOLI(control[0], control[2], target), strategy=new)
    
def plus1(circuit, l, countReg, workReg, control, ancilla, level):
    """
    Recursively add 1 to the LSB of a register and carries to all bits, if control == 1
    l: number of qubits in count register
    countReg, workReg: count register and associated work register
    control: control qubit to determine if plus1 should be executed
    ancilla: extra work qubit
    level: current qubit we are operating on, recursively travels from qubit 0 to l-1
    """
    # apply X to LSB
    if level == 0:
        circuit.append(CNOT(control, countReg[0]), strategy=new)
    if level < l - 1:
        # first level uses CNOT instead of TOFFOLI gate
        if level == 0:
            # move all X gates to first step to avoid unnecesarry gates
            circuit.append([X(qubit) for qubit in countReg], strategy=new)
            circuit.append(TOFFOLI(countReg[0], control, workReg[0]), strategy=new)
        else:
            circuit.append(TOFFOLI(countReg[level], workReg[level - 1], ancilla), strategy=new)
            circuit.append(TOFFOLI(ancilla, control, workReg[level]), strategy=new)
            circuit.append(TOFFOLI(countReg[level], workReg[level - 1], ancilla), strategy=new)

        circuit.append(TOFFOLI(workReg[level], control, countReg[level + 1]), strategy=new)
        # recursively call next layer
        plus1(circuit, l, countReg, workReg, control, ancilla, level + 1)
        # undo work qubits (exact opposite of first 7 lines - undoes calculation)
        if level == 0:
            circuit.append(TOFFOLI(countReg[0], control, workReg[0]), strategy=new)
            circuit.append([X(qubit) for qubit in countReg], strategy=new)
        else:
            circuit.append(TOFFOLI(countReg[level], workReg[level - 1], ancilla), strategy=new)
            circuit.append(TOFFOLI(ancilla, control, workReg[level]), strategy=new)
            circuit.append(TOFFOLI(countReg[level], workReg[level - 1], ancilla), strategy=new)

def generateParticleCounts(n_i, m, k):
    """Fill countsList with all combinations of n_phi, n_a, and n_b where each n lies in range [0, n_i+m-k],
    and the sum of all n's lies in range [n_i-k, m+n_i-k], all inclusive
    """
    countsList = []
    for numParticles in range(n_i-k, m+n_i-k+1):
        for numPhi in range(0, n_i+m-k+1):
            for numA in range(0, numParticles-numPhi+1):
                numB = numParticles - numPhi - numA
                countsList.append([numPhi, numA, numB])
    return countsList

def reverse(lst):
    """reverse a list in place"""
    lst.reverse()
    return lst

def intToBinary(l, number):
    """Converts integer to binary list of size l with LSB first and MSB last"""
    numberBinary = [int(x) for x in list('{0:0b}'.format(number))]
    numberBinary = (l - len(numberBinary)) * [0] + numberBinary
    return reverse(numberBinary)

def numberControl(circuit, l, number, countReg, workReg):
    """
    Applies an X to the l-2 (0 indexed) qubit of the work register if count register encodes the inputted number in binary
    returns this l-2 qubit, unless l=1, in which case return the only count register qubit
    DOES NOT CLEAN AFTER ITSELF - USE numberControlT to clean after this operation
    """
    if type(number) == int:
        numberBinary = intToBinary(l, number)
    else:
        numberBinary = number
    circuit.append([X(countReg[i]) for i in range(len(numberBinary)) if numberBinary[i] == 0], strategy=new)
    # first level does not use work qubits as control
    if l > 1:
        circuit.append(TOFFOLI(countReg[0], countReg[1], workReg[0]), strategy=new)
        # subfunction to recursively handle toffoli gates

    def binaryToffolis(level):
        circuit.append(TOFFOLI(countReg[level], workReg[level - 2], workReg[level - 1]), strategy=new)
        if level < l - 1:
            binaryToffolis(level + 1)

    if l > 2:
        binaryToffolis(2)
    # return qubit containing outcome of the operation
    if l == 1:
        return countReg[0]
    else:
        return workReg[l - 2]

def numberControlT(circuit, l, number, countReg, workReg):
    """CLEANS AFTER numberControl operation"""
    if type(number) == int:
        numberBinary = intToBinary(l, number)
    else:
        numberBinary = number

    # subfunction to recursively handle toffoli gates
    def binaryToffolisT(level):
        # circuit.append(TOFFOLI(countReg[level], workReg[level-2], workReg[level-1]), strategy=new)
        if level < l:
            binaryToffolisT(level + 1)
            # undo
            circuit.append(TOFFOLI(countReg[level], workReg[level - 2], workReg[level - 1]), strategy=new)

    if l > 2:
        binaryToffolisT(2)
        # undo
    if l > 1:
        circuit.append(TOFFOLI(countReg[0], countReg[1], workReg[0]), strategy=new)
        # undo
    circuit.append([X(countReg[i]) for i in range(len(numberBinary)) if numberBinary[i] == 0], strategy=new)

def generateGrayList(l, number):
    """
    l is the size of the current count register
    Return list of elements in gray code from |0> to |number> where each entry is of type[int, binary list].
    int: which bit is the target in the current iteration, binary list: the state of the rest of the qubits (controls)
    """
    grayList = [[0, l * [0]]]
    targetBinary = intToBinary(l, number)
    for index in range(len(targetBinary)):
        if targetBinary[index] == 1:
            grayList.append([index, (list(grayList[-1][1]))])
            grayList[-1][1][index] = 1
    return grayList[1:]

def twoLevelControlledRy(circuit, l, angle, k, externalControl, reg, workReg):
    """
    Implements two level Ry rotation from state |0> to |k>, if externalControl qubit is on
    for reference: http://www.physics.udel.edu/~msafrono/650/Lecture%206.pdf
    """
    grayList = generateGrayList(l, k)
    # handle the case where l=0 or 1
    if k==0:
        return
    if l == 1 and k == 1:
        circuit.append(cirq.ry(angle).controlled().on(externalControl, reg[0]))
        return

    # swap states according to Gray Code until one step before the end
    for element in grayList:
        targetQub = element[0]
        number = element[1]
        number = number[0:targetQub] + number[targetQub + 1:]
        controlQub = numberControl(circuit, l - 1, number, reg[0:targetQub] + reg[targetQub + 1:], workReg)
        if element == grayList[-1]:  # reached end
            circuit.append(TOFFOLI(controlQub, externalControl, workReg[l - 2]), strategy=new)
            circuit.append(cirq.ry(angle).controlled().on(workReg[l - 2], reg[targetQub]))
            circuit.append(TOFFOLI(controlQub, externalControl, workReg[l - 2]), strategy=new)
        else:  # swap states
            circuit.append(CNOT(controlQub, reg[targetQub]), strategy=new)
        numberControlT(circuit, l - 1, number, reg[0:targetQub] + reg[targetQub + 1:], workReg)

    # undo
    for element in reverse(grayList[:-1]):
        targetQub = element[0]
        number = element[1]
        number = number[0:targetQub] + number[targetQub + 1:]
        controlQub = numberControl(circuit, l - 1, number, reg[0:targetQub] + reg[targetQub + 1:], workReg)
        circuit.append(CNOT(controlQub, reg[targetQub]), strategy=new)
        numberControlT(circuit, l - 1, number, reg[0:targetQub] + reg[targetQub + 1:], workReg)
    return
        
def U_hAngle(flavor, n_phi, n_a, n_b, P_phi, P_a, P_b):
    """Determine angle of rotation used in U_h"""
    denominator = n_phi * P_phi + n_a * P_a + n_b * P_b
    if denominator == 0: # occurs if we are trying the case of no particles remaining (n_a = n_b = n_phi = 0)
        return 0
    flavorStringToP = {'phi': P_phi, 'a': P_a, 'b': P_b}
    emissionAmplitude = np.sqrt(flavorStringToP[flavor] / denominator)
    # correct for arcsin input greater than 1 errors for various input combinations that are irrelevant anyway
    emissionAmplitude = min(1, emissionAmplitude)
    return 2 * np.arcsin(emissionAmplitude)

def minus1(circuit, l, countReg, workReg, control, ancilla, level):
    """
    Recursively carries an subtraction of 1 to the LSB of a register to all bits if control == 1
    Equivalent to plus1 but with an X applied to all count qubits before and after gate
    """
    circuit.append([X(qubit) for qubit in countReg], strategy=new)
    plus1(circuit, l, countReg, workReg, control, ancilla, level)
    circuit.append([X(qubit) for qubit in countReg], strategy=new)

def U_h(circuit, l, n_i, m, n_phiReg, w_phiReg, n_aReg, w_aReg, n_bReg, w_bReg, wReg, eReg, pReg, hReg, w_hReg, P_phi,
        P_a, P_b):
    """Implement U_h from paper"""
    for k in range(n_i + m):
        countsList = generateParticleCounts(n_i, m, k)  # reduce the available number of particles
        for counts in countsList:
            n_phi, n_a, n_b = counts[0], counts[1], counts[2]
            # controlled R-y from |0> to |k> on all qubits with all possible angles depending on n_phi, n_a, n_b, and flavor
            for flavor in ['phi', 'a', 'b']:
                angle = U_hAngle(flavor, n_phi, n_a, n_b, P_phi, P_a, P_b)
                phiControl = numberControl(circuit, l, n_phi, n_phiReg, w_phiReg)
                aControl = numberControl(circuit, l, n_a, n_aReg, w_aReg)
                bControl = numberControl(circuit, l, n_b, n_bReg, w_bReg)
                circuit.append(TOFFOLI(phiControl, aControl, wReg[0]), strategy=new)
                circuit.append(TOFFOLI(bControl, wReg[0], wReg[1]), strategy=new)
                flavorControl(circuit, flavor, pReg[k], wReg[2], wReg[4]) # wReg[4] is work qubit but is reset to 0
                circuit.append(TOFFOLI(wReg[1], wReg[2], wReg[3]), strategy=new)
                circuit.append(TOFFOLI(eReg[0], wReg[3], wReg[4]), strategy=new)

                twoLevelControlledRy(circuit, l, angle, k+1, wReg[4], hReg[m], w_hReg)

                circuit.append(TOFFOLI(eReg[0], wReg[3], wReg[4]), strategy=new)  # next steps undo work qubits
                circuit.append(TOFFOLI(wReg[1], wReg[2], wReg[3]), strategy=new)
                flavorControl(circuit, flavor, pReg[k], wReg[2], wReg[4])
                circuit.append(TOFFOLI(bControl, wReg[0], wReg[1]), strategy=new)
                circuit.append(TOFFOLI(phiControl, aControl, wReg[0]), strategy=new)
                numberControlT(circuit, l, n_b, n_bReg, w_bReg)
                numberControlT(circuit, l, n_a, n_aReg, w_aReg)
                numberControlT(circuit, l, n_phi, n_phiReg, w_phiReg)

        # subtract from the counts register depending on which flavor particle emitted
        for flavor, countReg, workReg in zip(['phi', 'a', 'b'], [n_phiReg, n_aReg, n_bReg], [w_phiReg, w_aReg, w_bReg]):
            flavorControl(circuit, flavor, pReg[k], wReg[0], wReg[1])
            minus1(circuit, l, countReg, workReg, wReg[0], wReg[1], 0)
            flavorControl(circuit, flavor, pReg[k], wReg[0], wReg[1])

    # apply x on eReg if hReg[m] = 0, apply another x so we essentially control on not 0 instead of 0
    isZeroControl = numberControl(circuit, l, 0, hReg[m], w_hReg)
    circuit.append(CNOT(isZeroControl, eReg[0]))
    circuit.append(X(eReg[0]), strategy=new)
    numberControlT(circuit, l, 0, hReg[m], w_hReg)



# ============================
n_i, N = 1, 3
g_1, g_2, g_12 = 2, 1, 0
eps = .001
reps = 30
initialParticles = [[0,0,1]]
#initialParticles = [[0,0,1],[0,1,0]]
# ============================
    
# calculate constants
gp = math.sqrt(abs((g_1 - g_2) ** 2 + 4 * g_12 ** 2))
if g_1 > g_2:
    gp = -gp
g_a, g_b = (g_1 + g_2 - gp) / 2, (g_1 + g_2 + gp) / 2
u = math.sqrt(abs((gp + g_1 - g_2)/ (2 * gp)))
    
L = int(math.floor(math.log(N + n_i, 2)) + 1)

# evaluate P(Theta) and Delta(Theta) at every time step
timeStepList, P_aList, P_bList, P_phiList, Delta_aList, Delta_bList, Delta_phiList = [], [], [], [], [], [], []
populateParameterLists(N, timeStepList, P_aList, P_bList, P_phiList, Delta_aList, Delta_bList, Delta_phiList, g_a, g_b, eps)

# allocate and populate registers
pReg, hReg, w_hReg, eReg, wReg, n_aReg, w_aReg, n_bReg, w_bReg, n_phiReg, w_phiReg = [], [], [], [], [], [], [], [], [], [], []
allocateQubs(N, n_i, L, pReg, hReg, w_hReg, eReg, wReg, n_aReg, w_aReg, n_bReg, w_bReg, n_phiReg, w_phiReg)
qubits = {'pReg': pReg, 'hReg': hReg, 'w_hReg': w_hReg, 'eReg': eReg, 'wReg': wReg, 'n_aReg': n_aReg,
          'w_aReg': w_aReg, 'n_bReg': n_bReg, 'w_bReg': w_bReg, 'n_phiReg': n_phiReg, 'w_phiReg': w_phiReg}

# create circuit object and initialize particles
circuit = cirq.Circuit()
intializeParticles(circuit, pReg, initialParticles)
    
# begin stepping through subcircuits
for m in range(N):
    l = int(math.floor(math.log(m + n_i, 2)) + 1)

    # choose a particle to split (step 4)
    U_h(circuit, l, n_i, m, n_phiReg, w_phiReg, n_aReg, w_aReg, n_bReg, w_bReg, wReg, eReg, pReg, hReg, w_hReg,
        P_phiList[m], P_aList[m], P_bList[m])


cirq.ConvertToCzAndSingleGates().optimize_circuit(circuit)
#cirq.ExpandComposite().optimize_circuit(circuit)
#print(circuit)
#sys.stdout = open("test.svg", "w")
#print(circuit_to_svg(circuit))
#sys.stdout.close()


# ===========================================
from pytket.predicates import CompilationUnit
from pytket.passes import CommuteThroughMultis, DecomposeMultiQubitsIBM, RemoveRedundancies
from pytket.passes import SequencePass, RepeatPass, RepeatWithMetricPass, FullPeepholeOptimise
from pytket.cirq import cirq_to_tk, tk_to_cirq
from pytket.qiskit import tk_to_qiskit
from pytket.circuit import OpType

def cost(circ):
    return sum(pow(len(x.args),2) for x in circ)

seqpass = SequencePass([CommuteThroughMultis(), DecomposeMultiQubitsIBM(), RemoveRedundancies()])
#reppass = RepeatPass(seqpass)
reppass = RepeatWithMetricPass(seqpass, cost)
peephole = FullPeepholeOptimise()


tk_qc = cirq_to_tk(circuit)
nsqg = tk_qc.n_gates_of_type(OpType.PhasedX)+tk_qc.n_gates_of_type(OpType.X)+ \
    tk_qc.n_gates_of_type(OpType.H)+tk_qc.n_gates_of_type(OpType.T)+tk_qc.n_gates_of_type(OpType.Rz)
print('cirq decomposed                    : N(qubit) =',tk_qc.n_qubits,
      ', N(1q gate) =',nsqg,', N(CZ) =',tk_qc.n_gates_of_type(OpType.CZ))
#print(tk_qc)
cu_tk_qc = CompilationUnit(tk_qc)
reppass.apply(cu_tk_qc)
peephole.apply(cu_tk_qc)
opt_tk_qc = cu_tk_qc.circuit
nsqg = opt_tk_qc.n_gates_of_type(OpType.U1)+opt_tk_qc.n_gates_of_type(OpType.U2)+ \
    opt_tk_qc.n_gates_of_type(OpType.U3)
print('decomposed->tk opt                 : N(qubit) =',opt_tk_qc.n_qubits,
      ', N(1q gate) =',nsqg,', N(CX) =',opt_tk_qc.n_gates_of_type(OpType.CX))
#print(opt_tk_qc)
#sys.stdout = open("tk_test.svg", "w")
#print(circuit_to_svg(opt_tk_qc))
#sys.stdout.close()


from pytket.routing import Architecture
from pytket.circuit import Node
n = [Node('n',i) for i in range(27)]
arc_toronto = Architecture([[n[0],n[1]], [n[1],n[2]], [n[1],n[4]], [n[2],n[3]], [n[3],n[5]], [n[4],n[7]],
                            [n[5],n[8]], [n[6],n[7]], [n[7],n[10]], [n[8],n[9]], [n[8],n[11]], [n[10],n[12]], 
                            [n[11],n[14]], [n[12],n[13]], [n[12],n[15]], [n[13],n[14]], [n[14],n[16]], [n[15],n[18]], 
                            [n[16],n[19]], [n[17],n[18]], [n[18],n[21]], [n[19],n[20]], [n[19],n[22]], [n[21],n[23]], 
                            [n[22],n[25]], [n[23],n[24]], [n[24],n[25]], [n[25],n[26]]])

from pytket.device import Device
dev_toronto = Device(arc_toronto)                         


from pytket.passes import DefaultMappingPass, FullMappingPass, DecomposeSwapsToCXs
mapper = DefaultMappingPass(dev_toronto)
decomswap = DecomposeSwapsToCXs(dev_toronto)
peephole = FullPeepholeOptimise()
remred = RemoveRedundancies()

'''
cu2_tk_qc = CompilationUnit(tk_qc)
reppass.apply(cu2_tk_qc)
mapper.apply(cu2_tk_qc)
decomswap.apply(cu2_tk_qc)
#peephole.apply(cu2_tk_qc)
#reppass.apply(cu2_tk_qc)
#remred.apply(cu2_tk_qc)
tk_qc_toronto = cu2_tk_qc.circuit
nsqg = tk_qc_toronto.n_gates_of_type(OpType.PhasedX)+tk_qc_toronto.n_gates_of_type(OpType.X)+ \
    tk_qc_toronto.n_gates_of_type(OpType.H)+tk_qc_toronto.n_gates_of_type(OpType.T)+tk_qc_toronto.n_gates_of_type(OpType.Rz)
print('decomposed->tk opt,toronto         : N(qubit) =',tk_qc_toronto.n_qubits,
      ', N(1q gate) =',nsqg,', N(CX,CZ) =',tk_qc_toronto.n_gates_of_type(OpType.CX)+tk_qc_toronto.n_gates_of_type(OpType.CZ))
#print(tk_qc_toronto)
#c2 = tk_to_qiskit(tk_qc_toronto)
#print('tk_dev     : gates =',c2.count_ops(),' depth =', c2.depth())
#c2.draw(output='mpl',filename='tk_uh_dev.png')
'''


from pytket.backends.ibm import IBMQBackend
b = IBMQBackend(backend_name='ibmq_toronto', hub='ibm-q-utokyo', group='internal', project='icepp')
b.compile_circuit(tk_qc)
nsqg = tk_qc.n_gates_of_type(OpType.U1)+tk_qc.n_gates_of_type(OpType.U2)+tk_qc.n_gates_of_type(OpType.U3)
print('decomposed->backend                : N(qubit) =',opt_tk_qc.n_qubits,
      ', N(1q gate) =',nsqg,', N(CX) =',tk_qc.n_gates_of_type(OpType.CX))
#print(tk_qc)
c2 = tk_to_qiskit(tk_qc)
c2.draw(output='text',filename='tk_uh_backend.txt')
#c2.draw(output='mpl',filename='tk_uh_backend.pdf',scale=0.1)

'''
b.compile_circuit(tk_qc_toronto)
nsqg = tk_qc_toronto.n_gates_of_type(OpType.U1)+tk_qc_toronto.n_gates_of_type(OpType.U2)+\
    tk_qc_toronto.n_gates_of_type(OpType.U3)
print('decomposed->tk opt,toronto,backend : N(qubit) =',tk_qc_toronto.n_qubits,
      ', N(1q gate) =',nsqg,', N(CX) =',tk_qc_toronto.n_gates_of_type(OpType.CX))
#print(tk_qc_toronto)
#c2 = tk_to_qiskit(tk_qc_toronto)
#c2.draw(output='mpl',filename='tk_uh_toronto.png')
'''
