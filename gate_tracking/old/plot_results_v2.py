#!/usr/bin/env python

from array import array
import plot_root


genMu_R0, genMu_x0, genMu_y0 = array('d'), array('d'), array('d')
x_all, y_all, r_all, z_all = array('d'), array('d'), array('d'), array('d')
x_selected, y_selected, r_selected, z_selected = array('d'), array('d'), array('d'), array('d')
x_found, y_found, r_found, z_found = array('d'), array('d'), array('d'), array('d')

# -------------------
nMu = 5
Event_Num = 2

result_file = './truth_particle_info_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
result_file1 = './all_tplets_found_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
result_file2 = './selected_tplets_found_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
result_file3 = './qc_tplets_found_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
# -------------------


with open(result_file) as f:
    skip_line = f.readline()  # Number of GenMuons
    nmuon = int(skip_line)
    for l in range(nmuon):
        genMu_line = f.readline()
        mu_kin = genMu_line.split('\t')
        genMu_R0.append(float(mu_kin[1]))
        genMu_x0.append(float(mu_kin[2]))
        genMu_y0.append(float(mu_kin[3]))

with open(result_file1) as f:
    while 1:
        all_line = f.readline()  # n_all_tplet
        if all_line == "":
            break
        nall = int(all_line)
        for iq in range(nall):
            for il in range(3):
                hit_line = f.readline()  # i, x, y, z
                hit = hit_line.split('\t')
                x_all.append(float(hit[1]))
                y_all.append(float(hit[2]))
                r_all.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                z_all.append(float(hit[3]))

with open(result_file2) as f:
    while 1:
        s_line = f.readline()  # num_tplets
        if s_line == "":
            break
        num_tplets = int(s_line)
        for iq in range(num_tplets):
            for il in range(3):
                hit_line = f.readline()  # i, x, y, z
                hit = hit_line.split('\t')
                x_selected.append(float(hit[1]))
                y_selected.append(float(hit[2]))
                r_selected.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                z_selected.append(float(hit[3]))

with open(result_file3) as f:
    while 1:
        s_line = f.readline()  # ntplets_found
        if s_line == "":
            break
        ntplets_found = int(s_line)
        for iq in range(ntplets_found):
            for il in range(3):
                hit_line = f.readline()  # i, x, y, z
                hit = hit_line.split('\t')
                x_found.append(float(hit[1]))
                y_found.append(float(hit[2]))
                r_found.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                z_found.append(float(hit[3]))



plot_root.plotGraph('track_ising_v2_nMu'+str(nMu)+'_Event'+str(Event_Num), genMu_R0, genMu_x0, genMu_y0, 
                    x_all, y_all, r_all, z_all, 
                    x_selected, y_selected, r_selected, z_selected, 
                    x_found, y_found, r_found, z_found,  
                    1300, 1300, 1300, 1000)
plot_root.plotGraph('inner_track_ising_v2_nMu'+str(nMu)+'_Event'+str(Event_Num), genMu_R0, genMu_x0, genMu_y0, 
                    x_all, y_all, r_all, z_all, 
                    x_selected, y_selected, r_selected, z_selected, 
                    x_found, y_found, r_found, z_found, 
                    200, 200, 200, 200)
