#!/usr/bin/env python

# useful additional packages 
#import matplotlib.pyplot as plt
#import matplotlib.axes as axes
#%matplotlib inline
import numpy as np
import networkx as nx

from qiskit import Aer
from qiskit.tools.visualization import plot_histogram
from qiskit_aqua import Operator, run_algorithm
from qiskit_aqua.input import EnergyInput
#from qiskit_aqua.translators.ising import maxcut, tsp
#from qiskit_aqua.translators.ising import maxcut
from qiskit_aqua.algorithms import VQE, ExactEigensolver
from qiskit_aqua.components.optimizers import SPSA
from qiskit_aqua.components.variational_forms import RY
from qiskit_aqua import QuantumInstance

# setup aqua logging
import logging
from qiskit_aqua import set_aqua_logging
# set_aqua_logging(logging.DEBUG)  # choose INFO, DEBUG to see the log

import ising_tracking


import time
tstart = time.time()

from array import array
#import plot_root

# -------------------------------------
# Number of muons
nMu = 5

# Input file 
input_files = ['output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi-3.1420_-2.5130_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi-2.5130_-1.8850_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi-1.8850_-1.2570_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi-1.2570_-0.6280_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi-0.6280_-0.0000_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi-0.0000_0.6280_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi0.6280_1.2570_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi1.2570_1.8850_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi1.8850_2.5130_N9_Nsl3.txt',
               'output_tplet_menger-xy_nMu'+str(nMu)+'_Eta-1.0_1.0_Phi2.5130_3.1420_N9_Nsl3.txt']


# Event number for calculation 
run_solver = True
Event_Num = 0   # = 0,1,2, ...

# Triplet selection 
#   - dtheta_diff < 0.05
dtheta_diff_cut = 0.05

# Interaction strength
conflict_weight = 1000  # for now...
# -------------------------------------


n_found_file = 0

for ifile in range(len(input_files)):

    t1 = time.time()

    n_all_tplet = 0
    b_ij = np.zeros((100,100))  # temporary...
    num_tplets = 0

    genMu_R0, genMu_x0, genMu_y0 = array('d'), array('d'), array('d')
    x_all, y_all, r_all, z_all = array('d'), array('d'), array('d'), array('d')
    x_selected, y_selected, r_selected, z_selected = array('d'), array('d'), array('d'), array('d')
    x_found, y_found, r_found, z_found = array('d'), array('d'), array('d'), array('d')


    # Find line number for each event
    l_Event = []
    with open(input_files[ifile]) as f:
        lines = f.readlines()
        lines_strip = [line.strip() for line in lines]
        l_Event = [i for i, line in enumerate(lines_strip) if 'Event' in line]
    #print l_Event


    with open(input_files[ifile]) as f:

        print
        # Skip events to get the target event
        for l in range(l_Event[Event_Num]):
            skip_line = f.readline()

        # Skip Event number and generated muon information for now    
        skip_line = f.readline()  # Event
        if ifile == 0: 
            print(skip_line)

        skip_line = f.readline()  # Number of GenMuons
        nmuon = int(skip_line)
        for l in range(nmuon):
            genMu_line = f.readline()
            mu_kin = genMu_line.split('\t')
            genMu_R0.append(float(mu_kin[3]))
            genMu_x0.append(float(mu_kin[4]))
            genMu_y0.append(float(mu_kin[5]))
        empty_line = f.readline()

        #print(genMu_R0)

        seed_menger_curve = []
        seed_curve_sign = []
        seed_dtheta_avg = []
        seed_dtheta_diff = []
        seed_sl = []
        seed_l_xyz = []


        ngroup_line = f.readline()  # m_nSeedLayers
        for igroup in range(int(ngroup_line)):

            seed_line = f.readline()  # m_fitNLayer, start_layer, nSeed_LBL 
            seed = seed_line.split('\t')
            n_all_tplet += int(seed[2])

            if int(seed[0]) == 3:
                for iseed in range(int(seed[2])):
                    req_line = f.readline()  # menger_curve, curve_sign, dtheta_avg, dtheta_diff
                    req = req_line.split('\t')

                    # dtheta_diff selection : if cut fails, skip the next 3 lines and continue
                    if float(req[3]) > dtheta_diff_cut:
                        for l in range(int(seed[0])):
                            skip_line = f.readline()
                            # check all input triplets just to make plot 
                            hit = skip_line.split('\t')
                            x_all.append(float(hit[1]))
                            y_all.append(float(hit[2]))
                            r_all.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                            z_all.append(float(hit[3]))
                        continue


                    seed_menger_curve.append(float(req[0]))
                    seed_curve_sign.append(int(req[1]))
                    seed_dtheta_avg.append(float(req[2]))
                    seed_dtheta_diff.append(float(req[3]))
                    seed_sl.append(int(seed[1]))

                    seed_hit = []
                    for il in range(int(seed[0])):
                        hit_line = f.readline()  # il, seed_x, seed_y, seed_z
                        hit = hit_line.split('\t')
                        seed_hit.append([float(hit[1]),float(hit[2]),float(hit[3])])
                        #print iseed, req[0], hit[1]
                        x_all.append(float(hit[1]))
                        y_all.append(float(hit[2]))
                        r_all.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                        z_all.append(float(hit[3]))
                        x_selected.append(float(hit[1]))
                        y_selected.append(float(hit[2]))
                        r_selected.append((float(hit[1])*float(hit[1])+float(hit[2])*float(hit[2]))**0.5)
                        z_selected.append(float(hit[3]))

                    seed_l_xyz.append(seed_hit)

            empty_line = f.readline()
            

        #print len(seed_menger_curve)
        #print seed_menger_curve[0], seed_curve_sign[0], seed_dtheta_avg[0], seed_dtheta_diff[0], seed_sl[0], seed_l_xyz[0]

        for i in range(len(seed_sl)):
            print("File",ifile," :",i,", seed_sl =",seed_sl[i],", triplet =",seed_l_xyz[i],", curve_sign=",seed_curve_sign[i])

        # Triplet candidates
        num_tplets = len(seed_menger_curve)
        print("File",ifile," : # of all triplets =", n_all_tplet, " (", len(x_all), "hits), # of selected triplets =", num_tplets)


        # Find interacton strength ---
        for i in range(num_tplets):
            for j in range(num_tplets):
                if i > j:
                    continue
                elif i == j:
                    b_ij[i][j] = 0.01  # bias weight
                    continue

                #... if triplets have the same start layers ...
                if seed_sl[i] == seed_sl[j]:

                    # check whether there are any shared hits 
                    shared_hits = False
                    for il in range(3):
                        shared = True
                        for ixyz in range(3):
                            if seed_l_xyz[i][il][ixyz] != seed_l_xyz[j][il][ixyz]:
                                shared = False
                        if shared == True:
                            shared_hits = True

                    # if no shared hits --> unrelated triplets
                    if shared_hits == False:
                        b_ij[i][j] = 0
                    # if shared hits found --> conflicting triplets
                    else:
                        b_ij[i][j] = conflict_weight
 
                #... if triplets have start layers shifted by 1 ...
                elif seed_sl[i] == seed_sl[j] - 1:
                
                    # check whether there are any shared hits 
                    shared_hits = False
                    shared_layers = []
                    for il in range(2):
                        shared = True
                        for ixyz in range(3):
                            if seed_l_xyz[i][il+1][ixyz] != seed_l_xyz[j][il][ixyz]:
                                shared = False
                        if shared == True:
                            shared_hits = True
                            shared_layers.append(il)
                        
                    # if no shared hits --> unrelated triplets
                    if shared_hits == False:
                        #print "unrelated : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                        b_ij[i][j] = 0
                    # if shared hits found 
                    else:
                        #print "shared    : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                        # if shared at only one layer --> conflicting triplets
                        if len(shared_layers) == 1:
                            #print "shared(1)" #         : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                            b_ij[i][j] = conflict_weight

                        # if shared at two layers --> possible quadruplet candidates
                        elif len(shared_layers) == 2:                
                            #print "shared(2)     : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                            # check menger curvatures and signs
                            dcurve = abs(seed_menger_curve[i] - seed_menger_curve[j])
                            dcurve_sign = seed_curve_sign[i] * seed_curve_sign[j]
                            drz = abs(seed_dtheta_avg[i] - seed_dtheta_avg[j])

                            # if menger curvatures have opposite sign --> conflicting triplets (OK?)
                            if dcurve_sign == -1:
                                #print "shared(2,invalid)" # : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                                b_ij[i][j] = conflict_weight
                            # if curvatures are consistent --> good quadruplet candidates
                            else:
                                s_ij = -1 * (1 - 0.5 * (dcurve + drz))
                                b_ij[i][j] = s_ij
                                #print "shared(2,valid)    --> s_ij = ", s_ij #   : ", seed_l_xyz[i], "  ", seed_l_xyz[j]
                            
                        else:
                            print ("shared(other)    : ", seed_l_xyz[i], "  ", seed_l_xyz[j])
                                                    

                #... if triplets have start layers shifted by 2 or more ...
                elif seed_sl[i] <= seed_sl[j] - 2:
                    b_ij[i][j] = 0  # --> unrelated triplets


    print("# of interaction weights =", len(b_ij))
    print("") 


    J_ij = np.zeros((num_tplets,num_tplets))
    for i in range(num_tplets):
        for j in range(num_tplets):
            if i >= j:
                continue        
            J_ij[i][j] = b_ij[i][j]
    
    h_i = np.zeros(num_tplets)
    for i in range(num_tplets):
        bias = 0
        for k in range(num_tplets):
            bias += b_ij[i][k]+b_ij[k][i]
        bias *= -1
        h_i[i] = bias

        
    if run_solver == False:
        continue
    if num_tplets == 0:
        continue
    n_found_file += 1

    # === Solving Ising problem =====================

    qubitOp, offset = ising_tracking.get_ising_qubitops(J_ij,h_i)
    print("")
    print("offset=",offset)
    algo_input = EnergyInput(qubitOp)

    # Making the Hamiltonian in its full form and getting the lowest eigenvalue and eigenvector

    # ---  run optimization with default setting
    #ee = ExactEigensolver(qubitOp, k=1)
    #result = ee.run()

    '''
    # ---  run optimization with customized setting
    algorithm_cfg = {
        'name': 'ExactEigensolver',
    }
    params = {
        'problem': {'name': 'ising'},
        'algorithm': algorithm_cfg
    }
    result = run_algorithm(params,algo_input)
    '''

    # --- run optimization with VQE
    seed = 10598

    '''
    spsa = SPSA(max_trials=500)
    ry = RY(qubitOp.num_qubits, depth=5, entanglement='linear')
    #vqe = VQE(qubitOp, ry, spsa, 'matrix')
    vqe = VQE(qubitOp, ry, spsa, 'paulis')
    backend = Aer.get_backend('statevector_simulator')
    #backend = Aer.get_backend('qasm_simulator')
    quantum_instance = QuantumInstance(backend=backend, shots=100, seed=seed, seed_mapper=seed)
    result = vqe.run(quantum_instance)
    '''

    algorithm_cfg = {
        'name': 'VQE',
        'operator_mode': 'paulis' 
    }
    optimizer_cfg = {
        'name': 'SPSA',
        'max_trials': 300
    }
    var_form_cfg = {
        'name': 'RY',
        'depth': 5,
        'entanglement': 'linear'
    }
    params = {
        'problem': {'name': 'ising', 'random_seed': seed},
        'algorithm': algorithm_cfg,
        'optimizer': optimizer_cfg,
        'variational_form': var_form_cfg
    }
    backend = Aer.get_backend('statevector_simulator')
    #backend = Aer.get_backend('qasm_simulator')
    result = run_algorithm(params, algo_input, backend=backend)


    # -----------------------------------------------
    print('energy:', result['energy'])
    #print('eigvecs:', result['eigvecs'][0])
    #print('tsp objective:', result['energy'] + offset)
    #print('time:', result['eval_time'])

    x = ising_tracking.sample_most_likely(result['eigvecs'][0])
    print('x:',x)
    #print('feasible:', ising_tracking.ising_feasible(x))
    #z = ising_tracking.get_ising_solution(x)
    #print('solution:', z)
    #print('solution objective:', ising_tracking.ising_value(z, J_ij))


    ntplets_found = 0
    for iq in range(len(x)):
        if int(x[iq]) == 1:
            #print seed_l_xyz[iq]
            for il in range(3):
                x_found.append(seed_l_xyz[iq][il][0])
                y_found.append(seed_l_xyz[iq][il][1])
                r_found.append((seed_l_xyz[iq][il][0]*seed_l_xyz[iq][il][0]+seed_l_xyz[iq][il][1]*seed_l_xyz[iq][il][1])**0.5)
                z_found.append(seed_l_xyz[iq][il][2])
            ntplets_found += 1
    print("# of triplets found at lowest energy =",ntplets_found)
    print("") 


    output_file = './truth_particle_info_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
    output_file1 = './all_tplets_found_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
    output_file2 = './selected_tplets_found_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
    output_file3 = './qc_tplets_found_nMu'+str(nMu)+'_Event'+str(Event_Num)+'.txt'
    if n_found_file == 1:
        with open(output_file,mode='w') as f:
            f.write(str(nmuon)+'\n')
            for l in range(nmuon):
                f.write(str(l)+'\t'+str(genMu_R0[l])+'\t'+str(genMu_x0[l])+'\t'+str(genMu_y0[l])+'\n')        
        with open(output_file1,mode='w') as f:
            f.write(str(n_all_tplet)+'\n')        
            for iq in range(n_all_tplet):
                for il in range(3):
                    f.write(str(iq)+'\t'+str(x_all[3*iq+il])+'\t'+str(y_all[3*iq+il])+'\t'+str(z_all[3*iq+il])+'\n')
        with open(output_file2,mode='w') as f:
            f.write(str(num_tplets)+'\n')        
            for iq in range(num_tplets):
                for il in range(3):
                    f.write(str(iq)+'\t'+str(x_selected[3*iq+il])+'\t'+str(y_selected[3*iq+il])+'\t'+str(z_selected[3*iq+il])+'\n')
        with open(output_file3,mode='w') as f:
            f.write(str(ntplets_found)+'\n')        
            for iq in range(ntplets_found):
                for il in range(3):
                    f.write(str(iq)+'\t'+str(x_found[3*iq+il])+'\t'+str(y_found[3*iq+il])+'\t'+str(z_found[3*iq+il])+'\n')

    else:
        with open(output_file1,mode='a') as f:
            f.write(str(n_all_tplet)+'\n')        
            for iq in range(n_all_tplet):
                for il in range(3):
                    f.write(str(iq)+'\t'+str(x_all[3*iq+il])+'\t'+str(y_all[3*iq+il])+'\t'+str(z_all[3*iq+il])+'\n')
        with open(output_file2,mode='a') as f:
            f.write(str(num_tplets)+'\n')        
            for iq in range(num_tplets):
                for il in range(3):
                    f.write(str(iq)+'\t'+str(x_selected[3*iq+il])+'\t'+str(y_selected[3*iq+il])+'\t'+str(z_selected[3*iq+il])+'\n')
        with open(output_file3,mode='a') as f:
            f.write(str(ntplets_found)+'\n')        
            for iq in range(ntplets_found):
                for il in range(3):
                    f.write(str(iq)+'\t'+str(x_found[3*iq+il])+'\t'+str(y_found[3*iq+il])+'\t'+str(z_found[3*iq+il])+'\n')

    t2 = time.time()
    elapsed_time = t2-t1
    print("--- Elapsed time :",elapsed_time," [sec]")



tend = time.time()
elapsed_time = tend-tstart
print("")
print("=== Total elapsed time :",elapsed_time," [sec]")


#p4,p5,p6,p7,p8,p9,p10,p11 = plot_root.checkEff(x_found, y_found, z_found)
'''
plot_root.plotGraph('track', genMu_R0, genMu_x0, genMu_y0, 
                    x_all, y_all, r_all, z_all, 
                    x_selected, y_selected, r_selected, z_selected, 
                    x_found, y_found, r_found, z_found,  
                    1300, 1300, 1300, 1000)
plot_root.plotGraph('inner_track', genMu_R0, genMu_x0, genMu_y0, 
                    x_all, y_all, r_all, z_all, 
                    x_selected, y_selected, r_selected, z_selected, 
                    x_found, y_found, r_found, z_found, 
                    200, 200, 200, 200)
'''    
