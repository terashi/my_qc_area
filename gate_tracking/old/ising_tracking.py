# -*- coding: utf-8 -*-

# Copyright 2018 IBM.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =============================================================================

""" Convert symmetric TSP instances into Pauli list
Deal with TSPLIB format. It supports only EUC_2D edge weight type.
See https://wwwproxy.iwr.uni-heidelberg.de/groups/comopt/software/TSPLIB95/
and http://elib.zib.de/pub/mp-testdata/tsp/tsplib/tsp/index.html
Design the tsp object `w` as a two-dimensional np.array
e.g., w[i, j] = x means that the length of a edge between i and j is x
Note that the weights are symmetric, i.e., w[j, i] = x always holds.
"""

import logging
from collections import OrderedDict, namedtuple

import numpy as np
import numpy.random as rand
#from qiskit.tools.qi.pauli import Pauli
from qiskit.quantum_info import Pauli

from qiskit_aqua import Operator

logger = logging.getLogger(__name__)

"""Instance data of TSP"""
TspData = namedtuple('TspData', 'name dim coord W_ijk')


def get_ising_qubitops(J_ij,h_i,penalty=1e6):
    """Generate Hamiltonian for TSP of a graph.

    Args:
        ins (TspData) : TSP data including coordinates and distances.
        penalty (float) : Penalty coefficient for the constraints

    Returns:
        operator.Operator, float: operator for the Hamiltonian and a
        constant shift for the obj function.

    """
    num_qubits = len(h_i)
    print("Number of qubits (selected triplets) =",num_qubits)
    pauli_list = []
    shift = 0

    zero = np.zeros(num_qubits, dtype=np.bool)
    for i in range(num_qubits):
        for j in range(num_qubits):
            if i >= j:
                continue
            if J_ij[i][j] == 0:
                continue
            shift += J_ij[i][j]
            print("J_ij[",i,"][",j,"]=",J_ij[i][j])
            vp = np.zeros(num_qubits, dtype=np.bool)
            vp[i] = True
            vp[j] = True
            pauli_list.append([J_ij[i][j], Pauli(vp, zero)])

    zero = np.zeros(num_qubits, dtype=np.bool)
    for i in range(num_qubits):
        print("h_i[",i,"]=",h_i[i])
        shift += h_i[i]
        vp = np.zeros(num_qubits, dtype=np.bool)
        vp[i] = True
        pauli_list.append([h_i[i], Pauli(vp, zero)])

    return Operator(paulis=pauli_list), shift


def ising_value(z, w):
    """Compute the TSP value of a solution.

    Args:
        z (list[int]): list of cities.
        w (numpy.ndarray): adjacency matrix.

    Returns:
        float: value of the cut.
    """
    ret = 0.0
    for i in range(len(z) - 1):
        ret += w[z[i], z[i + 1]]
    ret += w[z[-1], z[0]]
    return ret


def ising_feasible(x):
    """Check whether a solution is feasible or not.

    Args:
        x (numpy.ndarray) : binary string as numpy array.

    Returns:
        bool: feasible or not.
    """
    n = int(np.sqrt(len(x)))
    y = np.zeros((n, n))
    for i in range(n):
        for p in range(n):
            y[i, p] = x[i * n + p]
    for i in range(n):
        if sum(y[i, p] for p in range(n)) != 1:
            return False
    for p in range(n):
        if sum(y[i, p] for i in range(n)) != 1:
            return False
    return True


def get_ising_solution(x):
    """Get graph solution from binary string.

    Args:
        x (numpy.ndarray) : binary string as numpy array.

    Returns:
        list[int]: sequence of cities to traverse.
    """
    n = int(np.sqrt(len(x)))
    z = []
    for p in range(n):
        for i in range(n):
            if x[i * n + p] >= 0.999:
                assert len(z) == p
                z.append(i)
    return z


def sample_most_likely(state_vector):
    """Compute the most likely binary string from state vector.

    Args:
        state_vector (numpy.ndarray or dict): state vector or counts.

    Returns:
        numpy.ndarray: binary string as numpy.ndarray of ints.
    """
    if isinstance(state_vector, dict) or isinstance(state_vector, OrderedDict):
        # get the binary string with the largest count
        binary_string = sorted(state_vector.items(), key=lambda kv: kv[1])[-1][0]
        x = np.asarray([int(y) for y in reversed(list(binary_string))])
        return x
    else:
        n = int(np.log2(state_vector.shape[0]))
        k = np.argmax(np.abs(state_vector))
        x = np.zeros(n)
        for i in range(n):
            x[i] = k % 2
            k >>= 1
        return x
