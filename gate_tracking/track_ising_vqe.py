#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np
import networkx as nx
import logging
import time

from qiskit import Aer
from qiskit import QuantumCircuit, ClassicalRegister, QuantumRegister
from qiskit.circuit.library import TwoLocal
from qiskit.aqua.algorithms import VQE, NumPyMinimumEigensolver, NumPyEigensolver
from qiskit.optimization.applications.ising.common import sample_most_likely
from qiskit.aqua.components.optimizers import SPSA, COBYLA
from qiskit.aqua.components.variational_forms import RY
from qiskit.aqua import QuantumInstance

import ising_tracking


file_r = 'Q_model3_05pct_qubo9.txt'
from ast import literal_eval
with open(file_r, "r") as f:
    line = f.read()
    Q = literal_eval(line)
print("Q size =",len(Q))


n_qubits = 100

nvar = 0
key_i = []
b_ij = np.zeros((n_qubits,n_qubits))
for (k1, k2), v in Q.items():
#    print(k1,k2,v)
    if k1 == k2:
        b_ij[nvar][nvar] = v
        key_i.append(k1)
        nvar += 1

#print('nvar =',nvar)
for (k1, k2), v in Q.items():
    if k1 != k2:
        for i in range(nvar):
            for j in range(nvar):
                if k1 == key_i[i] and k2 == key_i[j]:
                    if i < j:
                        b_ij[i][j] = v
                    else:
                        b_ij[j][i] = v


J_ij = np.zeros((nvar,nvar))
for i in range(nvar):
    for j in range(nvar):
        if i >= j:
            continue        
        J_ij[i][j] = b_ij[i][j]
        if J_ij[i][j] == 0:
            continue
        
h_i = np.zeros(nvar)
for i in range(nvar):
    bias = 0
    for k in range(n_qubits):
        bias += b_ij[i][k]+b_ij[k][i]
    bias *= -1
    h_i[i] = bias
    if h_i[i] == 0:
        continue


# === Solving Ising problem =====================
qubitOp, offset = ising_tracking.get_ising_qubitops(J_ij,h_i)
print("")
print("total number of qubits = ",qubitOp.num_qubits)
print("offset = ",offset)


# Making the Hamiltonian in its full form and getting the lowest eigenvalue and eigenvector

# ---  run optimization with default setting
ee = NumPyMinimumEigensolver(qubitOp)
result = ee.run()

print('energy:', result.eigenvalue.real)
x = sample_most_likely(result.eigenstate)
print('x:',x)
