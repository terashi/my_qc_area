import numpy as np
from math import pi
from qiskit import QuantumCircuit, ClassicalRegister, QuantumRegister
from qiskit.compiler import transpile
from qiskit.transpiler import PassManager



nqubits = 9
qc = QuantumCircuit(nqubits+3)

#qc.mcx([0,1,2,3], 4)
#qc.mcx([1,2,3,4], 5)
#qc.mcx([2,3,4,5], 6)

qc.x(nqubits-1)
qc.mcu1(pi/4, [i for i in range(nqubits-1)], nqubits-1)
qc.x(nqubits-1)
qc.mcu1(pi/4, [i+1 for i in range(nqubits-1)], nqubits)
#qc.x(nqubits-1)
#qc.x(1)
#qc.mcu1(pi/4, [i+2 for i in range(nqubits-1)], nqubits+1)
#qc.x(nqubits-1)
#qc.mcu1(pi/4, [i+3 for i in range(nqubits-1)], nqubits+2)

#qc.draw(output='mpl',filename='mcx.png')


from qiskit.transpiler.passes import Unroller, Unroll3qOrMore, Optimize1qGates
pass1 = Unroller(['u1', 'u2', 'u3', 'cx'])
#pass1 = Unroll3qOrMore()
#pass2 = Optimize1qGates(['u1', 'u2', 'u3'])

pm = PassManager(pass1)
new_qc = pm.run(qc)
#new_qc.draw(output='mpl',filename='tp_mcx.png')
print('unroll     : gates =', new_qc.count_ops(),' depth =', new_qc.depth())

#pm = PassManager([pass1,pass2])
#new_qc2 = pm.run(qc)
#new_qc2.draw(output='mpl',filename='tp2_mcx.png')


#from qiskit.test.mock import FakeParis
#backend = FakeParis()
from qiskit import IBMQ
IBMQ.load_account()
provider0 = IBMQ.get_provider(hub='ibm-q-utokyo', group='internal', project='icepp')
backend = provider0.get_backend('ibmq_toronto')
new_qc_opt3 = transpile(new_qc, backend=backend, optimization_level=3)
#new_qc_opt3.draw(output='mpl',filename='tp_opt3_mcx.png')
print('opt3       : gates =', new_qc_opt3.count_ops(),' depth =', new_qc_opt3.depth())




from pytket.predicates import CompilationUnit
from pytket.passes import CommuteThroughMultis, DecomposeMultiQubitsIBM, RemoveRedundancies
from pytket.passes import SequencePass, RepeatPass, RepeatWithMetricPass, FullPeepholeOptimise
from pytket.qiskit import qiskit_to_tk, tk_to_qiskit

def cost(circ):
    return sum(pow(len(x.args),2) for x in circ)

seqpass = SequencePass([CommuteThroughMultis(), DecomposeMultiQubitsIBM(), RemoveRedundancies()])
#reppass = RepeatPass(seqpass)
reppass = RepeatWithMetricPass(seqpass, cost)


tk_qc = qiskit_to_tk(new_qc)
#print(tk_qc.get_commands())
cu_tk_qc = CompilationUnit(tk_qc)
reppass.apply(cu_tk_qc)
opt_tk_qc = cu_tk_qc.circuit
c1 = tk_to_qiskit(opt_tk_qc)
print('tk         : gates =',c1.count_ops(),' depth =', c1.depth())
#c1.draw(output='mpl',filename='tk_mcx.png')



from pytket.routing import Architecture
from pytket.circuit import Node
n = [Node('n',i) for i in range(27)]
arc_toronto = Architecture([[n[0],n[1]], [n[1],n[2]], [n[1],n[4]], [n[2],n[3]], [n[3],n[5]], [n[4],n[7]],
                            [n[5],n[8]], [n[6],n[7]], [n[7],n[10]], [n[8],n[9]], [n[8],n[11]], [n[10],n[12]], 
                            [n[11],n[14]], [n[12],n[13]], [n[12],n[15]], [n[13],n[14]], [n[14],n[16]], [n[15],n[18]], 
                            [n[16],n[19]], [n[17],n[18]], [n[18],n[21]], [n[19],n[20]], [n[19],n[22]], [n[21],n[23]], 
                            [n[22],n[25]], [n[23],n[24]], [n[24],n[25]], [n[25],n[26]]])

from pytket.device import Device
dev_toronto = Device(arc_toronto)                         


from pytket.passes import DefaultMappingPass, FullMappingPass, DecomposeSwapsToCXs
mapper = DefaultMappingPass(dev_toronto)
decomswap = DecomposeSwapsToCXs(dev_toronto)
peephole = FullPeepholeOptimise()
remred = RemoveRedundancies()

cu2_tk_qc = CompilationUnit(tk_qc)
reppass.apply(cu2_tk_qc)
mapper.apply(cu2_tk_qc)
decomswap.apply(cu2_tk_qc)
#peephole.apply(cu2_tk_qc)
#reppass.apply(cu2_tk_qc)
#remred.apply(cu2_tk_qc)
tk_qc_toronto = cu2_tk_qc.circuit
#print(tk_qc_toronto.get_commands())
c2 = tk_to_qiskit(tk_qc_toronto)
print('tk_dev     : gates =',c2.count_ops(),' depth =', c2.depth())
#c2.draw(output='mpl',filename='tk_mcx_dev.png')


from pytket.backends.ibm import IBMQBackend
b = IBMQBackend(backend_name='ibmq_toronto', hub='ibm-q-utokyo', group='internal', project='icepp')
b.compile_circuit(tk_qc_toronto)
c2 = tk_to_qiskit(tk_qc_toronto)
print('tk_dev(be) : gates =',c2.count_ops(),' depth =',c2.depth())
#c2.draw(output='mpl',filename='tk_mcx_toronto.png')
