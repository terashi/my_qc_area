import numpy as np
import matplotlib.pyplot as plt
import time

## Iris dataset
import pandas as pd
from sklearn import datasets
from sklearn.metrics import roc_curve, auc, roc_auc_score
#from scipy.interpolate import spline


# class label, lepton 1 pT, lepton 1 eta, lepton 1 phi, lepton 2 pT, lepton 2 eta, lepton 2 phi, 
# missing energy magnitude, missing energy phi, MET_rel, axial MET, M_R, M_TR_2, R, MT2, S_R, 
# M_Delta_R, dPhi_r_b, cos(theta_r1)
df = pd.read_csv("Files/SUSY_1M.csv",names=('isSignal','lep1_pt','lep1_eta','lep1_phi','lep2_pt','lep2_eta','lep2_phi','miss_ene','miss_phi','MET_rel','axial_MET','M_R','M_TR_2','R','MT2','S_R','M_Delta_R','dPhi_r_b','cos_theta_r1'))
#print(data.head())

nevt_offset=500000
uin=0
jobn=1
nevt = 100
niter = 50000
nvar = 7
if nvar == 3:
    SelectedFeatures = ['lep1_pt','lep2_pt','miss_ene']
elif nvar == 5:
    SelectedFeatures = ['lep1_pt','lep2_pt','miss_ene','M_TR_2','M_Delta_R']
elif nvar == 7:
    SelectedFeatures = ['lep1_pt','lep1_eta','lep2_pt','lep2_eta','miss_ene','M_TR_2','M_Delta_R']
opt='COBYLA_def'


y_train_label = df['isSignal'].values[nevt*(jobn-1):nevt*jobn]
y_test_label = df['isSignal'].values[nevt_offset+nevt*(jobn-1):nevt_offset+nevt*jobn]
x_train = df[SelectedFeatures].values[nevt*(jobn-1):nevt*jobn] 
x_test = df[SelectedFeatures].values[nevt_offset+nevt*(jobn-1):nevt_offset+nevt*jobn]
y_train = np.eye(2)[pd.Series(y_train_label,dtype='int32')] # one-hot representation - shape:(150, 2)
y_test = np.eye(2)[pd.Series(y_test_label,dtype='int32')] # one-hot representation - shape:(150, 2)
label_names = ['background','signal']

print 'train/test_sample_size = ',len(x_train),'/',len(x_test)

'''
# Plot training data
fig = plt.figure(figsize=(12, 8))
ax1 = fig.add_subplot(2, 2, 1)
ax2 = fig.add_subplot(2, 2, 2)
ax3 = fig.add_subplot(2, 2, 3)
for t in range(2):
    x=[]
    y=[]
    z=[]
    for i in range(len(x_train)):
        if df['isSignal'][i] == t:
            x.append(x_train[i,0])
            y.append(x_train[i,1])
            z.append(x_train[i,2])
    cm = [plt.cm.Paired([c]) for c in [0,6]]
    ax1.scatter(x, y, c=cm[t], edgecolors='k', label=label_names[t])
    ax2.scatter(x, z, c=cm[t], edgecolors='k', label=label_names[t])
    ax3.scatter(y, z, c=cm[t], edgecolors='k', label=label_names[t])
# label
fig.suptitle('VV dataset')
ax1.set_xlabel(SelectedFeatures[0])
ax1.set_ylabel(SelectedFeatures[1])
ax2.set_xlabel(SelectedFeatures[0])
ax2.set_ylabel(SelectedFeatures[2])
ax3.set_xlabel(SelectedFeatures[1])
ax3.set_ylabel(SelectedFeatures[2])
plt.legend()
#plt.show()
plt.savefig('Truth_SUSY_3d_'+str(nevt)+'evt.pdf')
'''

'''
# Plot
h = 10.  # step size in the mesh
X = x_test
x_min, x_max = np.floor(X[:, 0].min()*h)/h - 0.5/h, np.ceil(X[:, 0].max()*h)/h + 0.5/h
y_min, y_max = np.floor(X[:, 1].min()*h)/h - 0.5/h, np.ceil(X[:, 1].max()*h)/h + 0.5/h
z_min, z_max = np.floor(X[:, 2].min()*h)/h - 0.5/h, np.ceil(X[:, 2].max()*h)/h + 0.5/h
xx, yy, zz = np.meshgrid(np.arange(x_min, x_max+0.5/h, 0.5/h), np.arange(y_min, y_max+0.5/h, 0.5/h), np.arange(z_min, z_max+0.5/h, 0.5/h))
'''

now = time.ctime()
cnvtime = time.strptime(now)
print 'Start_Time : ',time.strftime("%Y/%m/%d %H:%M", cnvtime)

start_time = time.time()
from qcl_classification_new import QclClassification

# Random number
random_seed = 1210015208+jobn
np.random.seed(random_seed)

#b Circuit paramter
nqubit = len(SelectedFeatures)
c_depth = 3
num_class = 2
print 'nqubit =',nqubit
print

# Instance of QclClassification
qcl = QclClassification(nqubit, c_depth, num_class)

# Training with BFGS
res, theta_init, theta_opt = qcl.fit(x_train, y_train, uin_type=uin, maxiter=niter)
exec_time = time.time() - start_time
print 'Training finished: '+str(exec_time)+' s'


# Testing
qcl.set_input_state(x_test, uin)
Zprob = qcl.pred(theta_opt) # Update model parameter theta    
Zpred = np.argmax(Zprob, axis=1)
#print('Zprob =',Zprob)
#print('Zpred =',Zpred)
#print('y_test_label =',y_test_label)

# Check prediction for traincing sample (overfitting?)
qcl.set_input_state(x_train, uin)
Zprob_train = qcl.pred(theta_opt) # Update model parameter theta    
Zpred_train = np.argmax(Zprob_train, axis=1)


# Signal and Background efficiencies
if len(y_test_label) > 0 and len(y_test_label) == len(Zpred):
    n_sig = np.sum(y_test_label==1)
    n_bg = np.sum(y_test_label==0)
    n_sig_match = np.sum(y_test_label+Zpred==2)
    n_bg_match = np.sum(y_test_label+Zpred==0)
    print 'Sig eff=',1.*n_sig_match/n_sig,' BG eff=',1.*(n_bg-n_bg_match)/n_bg
exec_time = time.time() - start_time
print 'Testing finished:  '+str(exec_time)+' s'


prob_test_signal = Zprob[:,1]
fpr, tpr, thresholds = roc_curve(y_test_label, prob_test_signal, drop_intermediate=False)
roc_auc = auc(fpr, tpr)
plt.plot(fpr, tpr, color='darkorange', lw=2, label='Testing (area = %0.3f)' % roc_auc)
plt.plot([0, 0], [1, 1], color='navy', lw=2, linestyle='--')
plt.xlim([-0.05, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.legend(loc='lower right')
plt.savefig('ROC_SUSY_uin'+str(uin)+'_'+str(nqubit)+'d_'+str(nevt)+'evt_iter'+str(niter)+'_depth'+str(c_depth)+'_'+opt+'_run'+str(jobn)+'.pdf')

prob_train_signal = Zprob_train[:,1]
fpr_tr, tpr_tr, thresholds_tr = roc_curve(y_train_label, prob_train_signal, drop_intermediate=False)
roc_auc_tr = auc(fpr_tr, tpr_tr)
plt.plot(fpr_tr, tpr_tr, color='darkblue', lw=2, label='Training (area = %0.3f)' % roc_auc_tr)
plt.legend(loc='lower right')
plt.savefig('ROC2_SUSY_uin'+str(uin)+'_'+str(nqubit)+'d_'+str(nevt)+'evt_iter'+str(niter)+'_depth'+str(c_depth)+'_'+opt+'_run'+str(jobn)+'.pdf')

print ''
print 'AUC(training) =',roc_auc_tr
print 'AUC(testing) =',roc_auc

now = time.ctime()
cnvtime = time.strptime(now)
print 'End_Time : ',time.strftime("%Y/%m/%d %H:%M", cnvtime)


'''
fpr_new = np.linspace(fpr.min(),fpr.max(),300)
tpr_smooth = spline(fpr,tpr,fpr_new)
plt.plot(fpr_new, tpr_smooth)
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.grid()
plt.savefig('ROC2.pdf')
'''

'''
# Plot model prediction 
def decision_boundary(X, y, theta, title='(title here)'):
    fig = plt.figure(figsize=(12, 8))
    ax1 = fig.add_subplot(2, 2, 1)
    ax2 = fig.add_subplot(2, 2, 2)
    ax3 = fig.add_subplot(2, 2, 3)

    # Plot the decision boundary. For that, we will assign a color to each
    # point in the mesh [x_min, x_max]x[y_min, y_max].
    qcl.set_input_state(np.c_[xx.ravel(), yy.ravel(), zz.ravel()])
    Z = qcl.pred(theta) # Update model parameter theta    
    Z = np.argmax(Z, axis=1)
    
    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    #plt.pcolormesh(xx, yy, Z, cmap=plt.cm.Paired)
    print "xx.shape=",xx.shape,", yy.shape=",yy.shape,", zz.shape=",zz.shape,", Z.shape=",Z.shape
    #print "xx,yy,Z :",len(xx[0]),len(yy[:,0]),len(Z[0]),len(Z[:,0])

    label_pred = []
    for i in range(len(X[:,0])):
        ix_match=0
        iy_match=0
        iz_match=0
        for ix in range(len(xx[0])-1):
            if X[:,0][i] > xx[0][:,0][ix] and X[:,0][i] <= xx[0][:,0][ix+1]:
                ix_match = ix
        for iy in range(len(yy[:,0])-1):
            if X[:,1][i] > yy[:,0][:,0][iy] and X[:,1][i] <= yy[:,0][:,0][iy+1]:
                iy_match = iy
        for iz in range(len(zz[0][0])-1):
            if X[:,2][i] > zz[0][0][iz] and X[:,2][i] <= zz[0][0][iz+1]:
                iz_match = iz
        label_pred.append(Z[iy_match][ix_match][iz_match])
                        
    #print "label_pred=",len(label_pred),label_pred
    n_sig=0
    n_bg=0
    n_sig_match=0
    n_bg_match=0
    if len(y) > 0 and len(y) == len(label_pred):
        n_sig = np.count_nonzero(y==1)
        n_bg = np.count_nonzero(y==0)
        for i in range(len(y)):
            if label_pred[i] == 1:
                if y[i] == 1:
                    n_sig_match = n_sig_match + 1
                elif y[i] == 0:
                    n_bg_match = n_bg_match + 1
    if n_sig > 0 and n_bg > 0:
        print 'Sig eff=',1.*n_sig_match/n_sig,' BG eff=',1.*n_bg_match/n_bg


    # Plot also the training points
    #plt.scatter(X[:, 0], X[:, 1], c=y, edgecolors='k', cmap=plt.cm.Paired)
    ax1.scatter(X[:, 0], X[:, 1], c=label_pred, edgecolors='k', cmap=plt.cm.Paired)
    ax2.scatter(X[:, 0], X[:, 2], c=label_pred, edgecolors='k', cmap=plt.cm.Paired)
    ax3.scatter(X[:, 1], X[:, 2], c=label_pred, edgecolors='k', cmap=plt.cm.Paired)

    # label
    fig.suptitle(title)
    ax1.set_xlabel(SelectedFeatures[0])
    ax1.set_ylabel(SelectedFeatures[1])
    ax2.set_xlabel(SelectedFeatures[0])
    ax2.set_ylabel(SelectedFeatures[2])
    ax3.set_xlabel(SelectedFeatures[1])
    ax3.set_ylabel(SelectedFeatures[2])
    #plt.show()
    #plt.legend()
    plt.savefig(title+'_SUSY_3d_'+str(nevt)+'evt_iter'+str(niter)+'_depth'+str(c_depth)+'.pdf')


# Plot with initial parameter
#decision_boundary(x_train, y_train_label, theta_init, title='Initial')
#exec_time = time.time() - start_time
#print 'Initial parameter set : '+str(exec_time)+' s'

# Plot with optimized parameter
decision_boundary(X, y_test_label, theta_opt, title='Optimized')
exec_time = time.time() - start_time
print 'Optimized parameter set : '+str(exec_time)+' s'
'''
