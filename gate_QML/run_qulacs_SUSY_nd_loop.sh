#!/bin/bash

#source ./setup_lcg94.sh

njob=10
uin_type=(1)
nevt=(100)
niter=(50000)
ndepth=(3)
nqubit=(3 5 7)
opt="COBYLA_def"


for uin in ${uin_type[@]}; do
    for evt in ${nevt[@]}; do
	for iter in ${niter[@]}; do
	    for depth in ${ndepth[@]}; do
		for qubit in ${nqubit[@]}; do
		    for ijob in $(seq ${njob}); do
			rnum=`date "+%-m%d%H%M%S"`
			sed s/NEVT/$evt/ qcl_SUSY_nd_master_loop.py | sed s/UIN/$uin/ | sed s/NITER/$iter/ | sed s/NDEPTH/$depth/ | sed s/OPTION/$opt/ | sed s/NVAR/${qubit}/ | sed s/JOBN/${ijob}/ | sed s/RNUM/${rnum}/ > ./run_susy_uin${uin}_${qubit}d_${evt}evt_iter${iter}_depth${depth}_${opt}_run${ijob}.py
			log=qulacs_SUSY_uin${uin}_${qubit}d_${evt}evt_iter${iter}_depth${depth}_${opt}_run${ijob}.log
			if [ -e ${log} ]; then
			    echo ${log}" file exist!"
			    exit
			fi
			python ./run_susy_uin${uin}_${qubit}d_${evt}evt_iter${iter}_depth${depth}_${opt}_run${ijob}.py >& ${log}
		    done
		done
	    done
	done
    done
done
