from qiskit import BasicAer
from qiskit.aqua import run_algorithm, QuantumInstance
from qiskit.aqua.algorithms import VQC
from qiskit.aqua.components.optimizers import SPSA, COBYLA
from qiskit.aqua.components.feature_maps import SecondOrderExpansion, FirstOrderExpansion
from qiskit.aqua.components.variational_forms import RYRZ
from qiskit.aqua.utils import split_dataset_to_data_and_labels, map_label_to_class_name
from qiskit.aqua.input import ClassificationInput

from qiskit.ignis.mitigation.measurement import CompleteMeasFitter, TensoredMeasFitter


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import time

import logging
from qiskit.aqua import set_qiskit_aqua_logging
set_qiskit_aqua_logging(logging.DEBUG)  # choose INFO, DEBUG to see the log

# class label, lepton 1 pT, lepton 1 eta, lepton 1 phi, lepton 2 pT, lepton 2 eta, lepton 2 phi, missing energy magnitude, missing energy phi, MET_rel, axial MET, M_R, M_TR_2, R, MT2, S_R, M_Delta_R, dPhi_r_b, cos(theta_r1)
df = pd.read_csv("Files/SUSY_1K.csv",names=('isSignal','lep1_pt','lep1_eta','lep1_phi','lep2_pt','lep2_eta','lep2_phi','miss_ene','miss_phi','MET_rel','axial_MET','M_R','M_TR_2','R','MT2','S_R','M_Delta_R','dPhi_r_b','cos_theta_r1'))


feature_dim = 3   # dimension of each data point
SelectedFeatures = ['lep1_pt', 'lep2_pt', 'miss_ene']

training_size = 10
testing_size = 10
random_seed = 10598
shots = 1024
uin_depth = 1
uvar_depth = 1
niter = 10
# ------------------------
#backend_name = 'ibmq_rochester' # 53 qubit machine
backend_name = 'ibmq_qasm_simulator' # HPC simulator
# ------------------------
option = 'test'


df_sig = df.loc[df.isSignal==1, SelectedFeatures]
df_bkg = df.loc[df.isSignal==0, SelectedFeatures]

df_sig_training = df_sig.values[:training_size]
df_bkg_training = df_bkg.values[:training_size]
df_sig_test = df_sig.values[training_size:training_size+testing_size]
df_bkg_test = df_bkg.values[training_size:training_size+testing_size]
training_input = {'1':df_sig_training, '0':df_bkg_training}
test_input = {'1':df_sig_test, '0':df_bkg_test}
print('train_input =',training_input)
#print('test_input =',test_input)

datapoints, class_to_label = split_dataset_to_data_and_labels(test_input)

start_time = time.process_time()

from qiskit import IBMQ
IBMQ.load_account()
print("Available backends:",IBMQ.providers())
provider0 = IBMQ.get_provider(project='icepp')
print("Backends for project 'icepp':",provider0.backends())

# --- If you want to use simulator, the following line should be uncommented and 
# --- "backend = provider0.get_backend(backend_ame) should be commented out.

#backend = BasicAer.get_backend('qasm_simulator')
backend = provider0.get_backend(backend_name)

exec_time = time.process_time() - start_time
print('----------------------------------------------------')
print(f'Backend ready :            {exec_time:.2f}s')


optimizer = COBYLA(maxiter=niter, disp=True)

feature_map = FirstOrderExpansion(feature_dimension=feature_dim, depth=uin_depth)
var_form = RYRZ(num_qubits=feature_dim, depth=uvar_depth)
vqc = VQC(optimizer, feature_map, var_form, training_input, test_input)

quantum_instance = QuantumInstance(backend, shots=shots, seed_simulator=random_seed, seed_transpiler=random_seed, skip_qobj_validation=True)
#quantum_instance = QuantumInstance(backend, shots=shots, seed_simulator=random_seed, optimization_level=3, seed_transpiler=random_seed, skip_qobj_validation=True)
#quantum_instance = QuantumInstance(backend, shots=shots, seed_simulator=random_seed, optimization_level=3, measurement_error_mitigation_cls=CompleteMeasFitter, seed_transpiler=random_seed, skip_qobj_validation=True)
#quantum_instance = QuantumInstance(backend, shots=shots, seed_simulator=random_seed, initial_layout=[42,43,44], seed_transpiler=random_seed, skip_qobj_validation=True)
#quantum_instance = QuantumInstance(backend, shots=shots, seed_simulator=random_seed, initial_layout=[42,43,44], measurement_error_mitigation_cls=CompleteMeasFitter, seed_transpiler=random_seed, skip_qobj_validation=True)

exec_time = time.process_time() - start_time
print(f'VQC instances ready :      {exec_time:.2f}s')

result = vqc.run(quantum_instance)
exec_time = time.process_time() - start_time
print(f'VQC run finished :         {exec_time:.2f}s')

predicted_probs, predicted_labels = vqc.predict(datapoints[0])
predicted_classes = map_label_to_class_name(predicted_labels, vqc.label_to_class)
print(f'VQC prediction finished :  {exec_time:.2f}s')
print('----------------------------------------------------')
#print('predicted_probs =',predicted_probs)

n_sig = np.sum(datapoints[1]==1)
n_bg = np.sum(datapoints[1]==0)
n_sig_match = np.sum(datapoints[1]+predicted_labels==2)
n_bg_match = np.sum(datapoints[1]+predicted_labels==0)

print(" --- Testing success ratio: ", result['testing_accuracy'])
#print(" ---   Prediction:   {}".format(predicted_labels))
#print(" ---   Label:        {}".format(datapoints[1]))
print(" ---   Signal eff =",n_sig_match/n_sig, ", Background eff =",(n_bg-n_bg_match)/n_bg)


from sklearn.metrics import roc_curve, auc, roc_auc_score
prob_test_signal = predicted_probs[:,1]
fpr, tpr, thresholds = roc_curve(datapoints[1], prob_test_signal, drop_intermediate=False)
roc_auc = auc(fpr, tpr)
plt.plot(fpr, tpr, color='darkorange', lw=2, label='Testing (area = %0.2f)' % roc_auc)
plt.plot([0, 0], [1, 1], color='navy', lw=2, linestyle='--')
plt.xlim([-0.05, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.legend(loc="lower right")
plt.savefig('ROC_'+backend_name+'_'+str(feature_dim)+'d_'+str(training_size*2)+'evt_iter'+str(niter)+'_o1uin-depth'+str(uin_depth)+'_uvar-depth'+str(uvar_depth)+'_'+option+'.pdf')

datapoints_tr, class_to_label_tr = split_dataset_to_data_and_labels(training_input)
predicted_probs_tr, predicted_labels_tr = vqc.predict(datapoints_tr[0])
prob_train_signal = predicted_probs_tr[:,1]
fpr_tr, tpr_tr, thresholds_tr = roc_curve(datapoints_tr[1], prob_train_signal, drop_intermediate=False)
roc_auc_tr = auc(fpr_tr, tpr_tr)
plt.plot(fpr_tr, tpr_tr, color='darkblue', lw=2, label='Training (area = %0.2f)' % roc_auc_tr)
plt.legend(loc="lower right")
plt.savefig('ROC2_'+backend_name+'_'+str(feature_dim)+'d_'+str(training_size*2)+'evt_iter'+str(niter)+'_o1uin-depth'+str(uin_depth)+'_uvar-depth'+str(uvar_depth)+'_'+option+'.pdf')
